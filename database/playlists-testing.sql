SELECT * FROM tracks t 
JOIN playlists_to_tracks ptt ON t.track_id = ptt.track_id
JOIN playlists p ON p.playlist_id = ptt.playlist_id
JOIN albums alb ON t.album_id=alb.album_id
JOIN artists art ON alb.artist_id=art.artist_id
WHERE p.playlist_id = 29;

SELECT SUM(t.duration) FROM tracks t 
JOIN playlists_to_tracks ptt ON t.track_id = ptt.track_id
JOIN playlists p ON p.playlist_id = ptt.playlist_id
JOIN albums alb ON t.album_id=alb.album_id
JOIN artists art ON alb.artist_id=art.artist_id
WHERE p.playlist_id = 29;

SELECT SUM(t.duration) FROM tracks t 
JOIN playlists_to_tracks ptt ON t.track_id = ptt.track_id
JOIN playlists p ON p.playlist_id = ptt.playlist_id
JOIN albums alb ON t.album_id=alb.album_id
JOIN artists art ON alb.artist_id=art.artist_id
WHERE p.playlist_id = 29 AND t.genre_id = 6;
-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for playlist_generator
CREATE DATABASE IF NOT EXISTS `playlist_generator` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `playlist_generator`;

-- Dumping structure for table playlist_generator.albums
CREATE TABLE IF NOT EXISTS `albums` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `album_tracklist_url` varchar(100) NOT NULL,
  `artist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`album_id`),
  UNIQUE KEY `album_tracklist_URL` (`album_tracklist_url`),
  KEY `FK_albums_artists` (`artist_id`),
  CONSTRAINT `FK_albums_artists` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.albums: ~0 rows (approximately)
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.artists
CREATE TABLE IF NOT EXISTS `artists` (
  `artist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `artist_tracklist_url` varchar(100) NOT NULL,
  PRIMARY KEY (`artist_id`),
  UNIQUE KEY `artist_tracklist_URL` (`artist_tracklist_url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.artists: ~0 rows (approximately)
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) DEFAULT '',
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country` (`country`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;

-- Dumping data for table playlist_generator.countries: ~247 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`country_id`, `country`) VALUES
	(1, 'Afghanistan'),
	(2, 'Albania'),
	(3, 'Algeria'),
	(4, 'American Samoa'),
	(5, 'Andorra'),
	(6, 'Angola'),
	(7, 'Anguilla'),
	(8, 'Antarctica'),
	(9, 'Antigua and Barbuda'),
	(10, 'Argentina'),
	(11, 'Armenia'),
	(12, 'Aruba'),
	(13, 'Australia'),
	(14, 'Austria'),
	(15, 'Azerbaijan'),
	(16, 'Bahamas'),
	(17, 'Bahrain'),
	(18, 'Bangladesh'),
	(19, 'Barbados'),
	(20, 'Belarus'),
	(21, 'Belgium'),
	(22, 'Belize'),
	(23, 'Benin'),
	(24, 'Bermuda'),
	(25, 'Bhutan'),
	(26, 'Bolivia'),
	(27, 'Bosnia and Herzegovina'),
	(28, 'Botswana'),
	(29, 'Bouvet Island'),
	(30, 'Brazil'),
	(31, 'British Indian Ocean Territory'),
	(32, 'Brunei Darussalam'),
	(33, 'Bulgaria'),
	(34, 'Burkina Faso'),
	(35, 'Burundi'),
	(36, 'Cambodia'),
	(37, 'Cameroon'),
	(38, 'Canada'),
	(39, 'Cape Verde'),
	(40, 'Cayman Islands'),
	(41, 'Central African Republic'),
	(42, 'Chad'),
	(43, 'Chile'),
	(44, 'China'),
	(45, 'Christmas Island'),
	(46, 'Cocos (Keeling) Islands'),
	(47, 'Colombia'),
	(48, 'Comoros'),
	(49, 'Congo'),
	(50, 'Cook Islands'),
	(51, 'Costa Rica'),
	(52, 'Croatia (Hrvatska)'),
	(53, 'Cuba'),
	(54, 'Cyprus'),
	(55, 'Czech Republic'),
	(56, 'Denmark'),
	(57, 'Djibouti'),
	(58, 'Dominica'),
	(59, 'Dominican Republic'),
	(60, 'East Timor'),
	(61, 'Ecuador'),
	(62, 'Egypt'),
	(63, 'El Salvador'),
	(64, 'Equatorial Guinea'),
	(65, 'Eritrea'),
	(66, 'Estonia'),
	(67, 'Ethiopia'),
	(68, 'Falkland Islands (Malvinas)'),
	(69, 'Faroe Islands'),
	(70, 'Fiji'),
	(71, 'Finland'),
	(72, 'France'),
	(73, 'France, Metropolitan'),
	(74, 'French Guiana'),
	(75, 'French Polynesia'),
	(76, 'French Southern Territories'),
	(77, 'Gabon'),
	(78, 'Gambia'),
	(79, 'Georgia'),
	(80, 'Germany'),
	(81, 'Ghana'),
	(82, 'Gibraltar'),
	(84, 'Greece'),
	(85, 'Greenland'),
	(86, 'Grenada'),
	(87, 'Guadeloupe'),
	(88, 'Guam'),
	(89, 'Guatemala'),
	(83, 'Guernsey'),
	(90, 'Guinea'),
	(91, 'Guinea-Bissau'),
	(92, 'Guyana'),
	(93, 'Haiti'),
	(94, 'Heard and Mc Donald Islands'),
	(95, 'Honduras'),
	(96, 'Hong Kong'),
	(97, 'Hungary'),
	(98, 'Iceland'),
	(99, 'India'),
	(101, 'Indonesia'),
	(102, 'Iran (Islamic Republic of)'),
	(103, 'Iraq'),
	(104, 'Ireland'),
	(100, 'Isle of Man'),
	(105, 'Israel'),
	(106, 'Italy'),
	(107, 'Ivory Coast'),
	(109, 'Jamaica'),
	(110, 'Japan'),
	(108, 'Jersey'),
	(111, 'Jordan'),
	(112, 'Kazakhstan'),
	(113, 'Kenya'),
	(114, 'Kiribati'),
	(115, 'Korea, Democratic People\'s Republic of'),
	(116, 'Korea, Republic of'),
	(117, 'Kosovo'),
	(118, 'Kuwait'),
	(119, 'Kyrgyzstan'),
	(120, 'Lao People\'s Democratic Republic'),
	(121, 'Latvia'),
	(122, 'Lebanon'),
	(123, 'Lesotho'),
	(124, 'Liberia'),
	(125, 'Libyan Arab Jamahiriya'),
	(126, 'Liechtenstein'),
	(127, 'Lithuania'),
	(128, 'Luxembourg'),
	(129, 'Macau'),
	(130, 'Macedonia'),
	(131, 'Madagascar'),
	(132, 'Malawi'),
	(133, 'Malaysia'),
	(134, 'Maldives'),
	(135, 'Mali'),
	(136, 'Malta'),
	(137, 'Marshall Islands'),
	(138, 'Martinique'),
	(139, 'Mauritania'),
	(140, 'Mauritius'),
	(141, 'Mayotte'),
	(142, 'Mexico'),
	(143, 'Micronesia, Federated States of'),
	(144, 'Moldova, Republic of'),
	(145, 'Monaco'),
	(146, 'Mongolia'),
	(147, 'Montenegro'),
	(148, 'Montserrat'),
	(149, 'Morocco'),
	(150, 'Mozambique'),
	(151, 'Myanmar'),
	(152, 'Namibia'),
	(153, 'Nauru'),
	(154, 'Nepal'),
	(155, 'Netherlands'),
	(156, 'Netherlands Antilles'),
	(157, 'New Caledonia'),
	(158, 'New Zealand'),
	(159, 'Nicaragua'),
	(160, 'Niger'),
	(161, 'Nigeria'),
	(162, 'Niue'),
	(163, 'Norfolk Island'),
	(164, 'Northern Mariana Islands'),
	(165, 'Norway'),
	(166, 'Oman'),
	(167, 'Pakistan'),
	(168, 'Palau'),
	(169, 'Palestine'),
	(170, 'Panama'),
	(171, 'Papua New Guinea'),
	(172, 'Paraguay'),
	(173, 'Peru'),
	(174, 'Philippines'),
	(175, 'Pitcairn'),
	(176, 'Poland'),
	(177, 'Portugal'),
	(178, 'Puerto Rico'),
	(179, 'Qatar'),
	(180, 'Reunion'),
	(181, 'Romania'),
	(182, 'Russian Federation'),
	(183, 'Rwanda'),
	(184, 'Saint Kitts and Nevis'),
	(185, 'Saint Lucia'),
	(186, 'Saint Vincent and the Grenadines'),
	(187, 'Samoa'),
	(188, 'San Marino'),
	(189, 'Sao Tome and Principe'),
	(190, 'Saudi Arabia'),
	(191, 'Senegal'),
	(192, 'Serbia'),
	(193, 'Seychelles'),
	(194, 'Sierra Leone'),
	(195, 'Singapore'),
	(196, 'Slovakia'),
	(197, 'Slovenia'),
	(198, 'Solomon Islands'),
	(199, 'Somalia'),
	(200, 'South Africa'),
	(201, 'South Georgia South Sandwich Islands'),
	(202, 'South Sudan'),
	(203, 'Spain'),
	(204, 'Sri Lanka'),
	(205, 'St. Helena'),
	(206, 'St. Pierre and Miquelon'),
	(207, 'Sudan'),
	(208, 'Suriname'),
	(209, 'Svalbard and Jan Mayen Islands'),
	(210, 'Swaziland'),
	(211, 'Sweden'),
	(212, 'Switzerland'),
	(213, 'Syrian Arab Republic'),
	(214, 'Taiwan'),
	(215, 'Tajikistan'),
	(216, 'Tanzania, United Republic of'),
	(217, 'Thailand'),
	(218, 'Togo'),
	(219, 'Tokelau'),
	(220, 'Tonga'),
	(221, 'Trinidad and Tobago'),
	(222, 'Tunisia'),
	(223, 'Turkey'),
	(224, 'Turkmenistan'),
	(225, 'Turks and Caicos Islands'),
	(226, 'Tuvalu'),
	(227, 'Uganda'),
	(228, 'Ukraine'),
	(229, 'United Arab Emirates'),
	(230, 'United Kingdom'),
	(231, 'United States'),
	(232, 'United States minor outlying islands'),
	(233, 'Uruguay'),
	(234, 'Uzbekistan'),
	(235, 'Vanuatu'),
	(236, 'Vatican City State'),
	(237, 'Venezuela'),
	(238, 'Vietnam'),
	(239, 'Virgin Islands (British)'),
	(240, 'Virgin Islands (U.S.)'),
	(241, 'Wallis and Futuna Islands'),
	(242, 'Western Sahara'),
	(243, 'Yemen'),
	(244, 'Zaire'),
	(245, 'Zambia'),
	(246, 'Zimbabwe');
-- Dumping data for table playlist_generator.countries: ~0 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.genres
CREATE TABLE IF NOT EXISTS `genres` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deezer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `deezer_id` (`deezer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.genres: ~0 rows (approximately)
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.playlists
CREATE TABLE IF NOT EXISTS `playlists` (
  `playlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `picture` mediumblob DEFAULT NULL,
  PRIMARY KEY (`playlist_id`),
  KEY `FK_user_id` (`user_id`),
  CONSTRAINT `FK_playlists_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.playlists: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.playlists_to_genres
CREATE TABLE IF NOT EXISTS `playlists_to_genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_playlist_to_genres_playlists` (`playlist_id`),
  KEY `FK_playlist_to_genres_genres` (`genre_id`),
  CONSTRAINT `FK_playlist_to_genres_genres` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`genre_id`) ON DELETE NO ACTION,
  CONSTRAINT `FK_playlist_to_genres_playlists` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.playlists_to_genres: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists_to_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists_to_genres` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.playlists_to_tracks
CREATE TABLE IF NOT EXISTS `playlists_to_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL DEFAULT 0,
  `track_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_playlists_to_tracks_playlists` (`playlist_id`),
  KEY `FK_playlists_to_tracks_tracks` (`track_id`),
  CONSTRAINT `FK_playlists_to_tracks_playlists` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`playlist_id`),
  CONSTRAINT `FK_playlists_to_tracks_tracks` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.playlists_to_tracks: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists_to_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists_to_tracks` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `name`) VALUES
	(2, 'ROLE_ADMIN'),
	(1, 'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.tracks
CREATE TABLE IF NOT EXISTS `tracks` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `link` varchar(50) NOT NULL,
  `duration` int(5) NOT NULL,
  `rank` int(11) NOT NULL,
  `preview_url` varchar(100) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`track_id`),
  UNIQUE KEY `link` (`link`),
  KEY `FK_tracks_albums` (`album_id`),
  KEY `FK_tracks_genres` (`genre_id`),
  CONSTRAINT `FK_tracks_albums` FOREIGN KEY (`album_id`) REFERENCES `albums` (`album_id`),
  CONSTRAINT `FK_tracks_genres` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.tracks: ~0 rows (approximately)
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table playlist_generator.users_to_roles
CREATE TABLE IF NOT EXISTS `users_to_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_to_roles_users` (`user_id`),
  KEY `FK_users_to_roles_roles` (`role_id`),
  CONSTRAINT `FK_users_to_roles_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `FK_users_to_roles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table playlist_generator.users_to_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_to_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_to_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
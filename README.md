# Playlist Generator
#### A web application for drivers who like to listen to music. The driver inputs the starting and ending location and the application generates a playlist for him/her to listen while driving. The driver also can input a preference in genres for his/her playlist.

## Table of contents

* [General Info](#general-info)
* [Build](#build)
* [Public Part](#public-part)
* [Private Part](#private-part-users-only)
* [Administration Part](#administration-part)
* [External Services](#external-services)
* [Features](#features)
* [URL](#url)



## General Info

Playlist Generator calculates the travel duration time between the starting and destination locations and combines tracks chosen randomly in the specified genres, until the playing time roughly matches the travel time duration.

## Build
All you need to start the application is to open it with IntelliJ IDEA Ultimate. The rest will be done automatically.

## Public Part

The public part is visible without authentication. This includes the application start page, the user login and user registration forms, as well as the list of all user generated playlists. Users can also play those playlists. People that are not authenticated can't use the main functionality of the application - creating a playlist for their travel.

## Private Part (Users Only)

The private part is accessible only to users who have successfully authenticated (registered users).
The private part of the web application provides the users the ability to generate new playlists, control the generation algorithm, edit or delete their own existing playlists.
Editing existing playlists is limited to changing the title or associated genre tags, but does not include editing of the tracklist (e.g. removing or adding individual songs).

## Administration Part

Users with the system administrator role can administer all major information objects. The administrators have the following functionality

* CRUD over users and other administrators
* CRUD over the playlists

## External Services

The Playlist Generator web application consume two public REST services in order to achieve the main functionality. And another one for getting random images for playlists.

### Microsoft Bing Maps External Service

We will use this external service to help us calculate the travel duration between two addresses.
Official doc: https://docs.microsoft.com/en-us/bingmaps/rest-services/routes/calculate-a-distance-matrix

### Deezer External Service

Deezer is an audio streaming service, which will help us search and gather the tracks needed for the playlist generation.
Official doc: https://developers.deezer.com/login?redirect=/api

### Pixabay External Service

Over 1 million+ high quality stock images. Official doc: https://pixabay.com/api/docs/


## Features

* Playlist

![Example screenshot](./img/playlist.png)


* Admin Panel

![Example screenshot](./img/admin-panel.gif)

## URL
http://playlist-generator.eu-central-1.elasticbeanstalk.com/
Disclaimer: sync of genres doesn't work on the website, but on the localhost works fine!
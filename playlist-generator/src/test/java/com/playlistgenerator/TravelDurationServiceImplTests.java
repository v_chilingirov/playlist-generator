package com.playlistgenerator;

import com.playlistgenerator.models.Journey;
import com.playlistgenerator.models.Location;
import com.playlistgenerator.services.TravelDurationServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.env.MockEnvironment;

@RunWith(MockitoJUnitRunner.class)
public class TravelDurationServiceImplTests {

    @Mock
    MockEnvironment mockEnv;

    @InjectMocks
    TravelDurationServiceImpl service;

    private Journey journey;
    private Location startLocation;
    private Location endLocation;

    @Before
    public void init() {
        Mockito.when(mockEnv.getProperty("bingAPIkey"))
                .thenReturn("ArrJvcjkDeXHfIhBLQBn3tYPe22S6_wqe-eikaFISlc66tRLKTWrJZBUTgE9bghH");

        service = new TravelDurationServiceImpl(mockEnv);
    }

    @Test
    public void calculateTravelDuration_Should_Return97_When_Called() {
        //Arrange
        setWorkingLocations();
        createJourney(startLocation, endLocation);

        //Act
        service.calculateTravelDuration(journey);

        //Assert
        Assert.assertEquals(97, service.getTravelDuration());
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateTravelDuration_Should_Throw_When_CalledWithNonExistingAddress() {
        //Arrange
        setWorkingLocations();
        setWrongLocations();
        createJourney(startLocation, endLocation);


        //Act
        try {
            service.calculateTravelDuration(journey);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("That address doesn't exist", e.getMessage());
            throw e;
        }
    }

    private void createJourney(Location startLocation, Location endLocation) {
        journey = new Journey();
        journey.setStartLocation(startLocation);
        journey.setEndLocation(endLocation);
    }

    private void setWorkingLocations() {
        startLocation = new Location();
        startLocation.setCountryRegion("Bulgaria");
        startLocation.setLocality("Sofia");
        startLocation.setAdminDistrict("Sofia City");
        startLocation.setPostalCode("1784");
        startLocation.setAddressLine("Bulevard Aleksandar Malinov 31");

        endLocation = new Location();
        endLocation.setCountryRegion("Bulgaria");
        endLocation.setLocality("Chirpan");
        endLocation.setAdminDistrict("Stara Zagora");
        endLocation.setPostalCode("6200");
        endLocation.setAddressLine("Ulitsa Nov zhivot 15");
    }

    private void setWrongLocations() {
        endLocation = new Location();
        endLocation.setCountryRegion("asdasda");
        endLocation.setLocality("asdasdasd");
        endLocation.setAdminDistrict("");
        endLocation.setPostalCode("");
        endLocation.setAddressLine("");
    }
}

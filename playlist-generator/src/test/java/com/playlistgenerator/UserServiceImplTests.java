package com.playlistgenerator;

import com.playlistgenerator.exceptions.EmptyDatabaseException;
import com.playlistgenerator.models.*;
import com.playlistgenerator.repositories.contracts.UserRepository;
import com.playlistgenerator.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    private User user = new User();
    private Role role = new Role();

    @Test
    public void getAllUsers_Should_ReturnListWithUsers_When_Called() {
        //Arrange
        List<User> users = createUsers();

        Mockito.when(mockRepository.getAllUsers())
                .thenReturn(users);

        //Act
        List<User> result = service.getAllUsers();

        //Assert
        testsWithUsers(users, result);
    }

    @Test(expected = EmptyDatabaseException.class)
    public void getAllUsers_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Mockito.when(mockRepository.getAllUsers())
                .thenReturn(null);

        //Act
        service.getAllUsers();
    }

    @Test(expected = RuntimeException.class)
    public void getAllUsers_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getAllUsers())
                .thenThrow(RuntimeException.class);

        //Act
        service.getAllUsers();
    }

    @Test
    public void getAllDeletedUsers_Should_ReturnListWithDeletedUsers_When_Called() {
        //Arrange
        List<User> users = createUsers();

        Mockito.when(mockRepository.getAllDeletedUsers())
                .thenReturn(users);

        //Act
        List<User> result = service.getAllDeletedUsers();

        //Assert
        testsWithUsers(users, result);
    }

    @Test(expected = RuntimeException.class)
    public void getAllDeletedUsers_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getAllDeletedUsers())
                .thenThrow(RuntimeException.class);

        //Act
        service.getAllDeletedUsers();
    }

    @Test
    public void getUser_Should_ReturnUser_When_Called() {
        //Arrange
        Mockito.when(mockRepository.getUser("username"))
                .thenReturn(user);

        //Act
        User test = service.getUser("username");

        //Assert
        Assert.assertEquals(user, test);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUser_Should_Throw_When_SuchUserDoesntExist() {
        //Act
        service.getUser("username");
    }

    @Test(expected = RuntimeException.class)
    public void getUser_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getUser("test"))
                .thenThrow(RuntimeException.class);

        //Act
        service.getUser("test");
    }

    @Test
    public void getUserWithRoles_Should_ReturnUser_When_Called() {
        //Arrange
        user.setRoles(new HashSet<>());
        user.addRole(role);
        Mockito.when(mockRepository.getUserWithRoles("username"))
                .thenReturn(user);

        //Act
        User test = service.getUserWithRoles("username");

        //Assert
        Assert.assertEquals(user, test);
        Assert.assertEquals(1, test.getRoles().size());
        Assert.assertTrue(test.getRoles().contains(role));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserWithRoles_Should_Throw_When_SuchUserDoesntExist() {
        //Act
        service.getUserWithRoles("username");
    }

    @Test(expected = RuntimeException.class)
    public void getUserWithRoles_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getUserWithRoles("test"))
                .thenThrow(RuntimeException.class);

        //Act
        service.getUserWithRoles("test");
    }

    @Test
    public void getUserWithPlaylists_Should_ReturnUser_When_Called() {
        //Arrange
        List<Playlist> list = new ArrayList<>();
        Playlist playlist1 = new Playlist();
        Playlist playlist2 = new Playlist();
        playlist1.setId(1);
        playlist2.setId(2);
        list.add(playlist1);
        list.add(playlist2);
        user.setUsername("username");
        user.setPlaylists(list);
        Mockito.when(mockRepository.getUserWithPlayists("username"))
                .thenReturn(user);

        //Act
        User test = service.getUserWithPlaylists("username");

        //Assert
        Assert.assertEquals(user, test);
        Assert.assertEquals(test.getPlaylists(), user.getPlaylists());
        Assert.assertEquals(test.getPlaylists().get(0).getId(), user.getPlaylists().get(0).getId());
        Assert.assertEquals(test.getPlaylists().get(1).getId(), user.getPlaylists().get(1).getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserWithPlaylists_Should_Throw_When_SuchUserDoesntExist() {
        //Act
        service.getUserWithPlaylists("username");
    }

    @Test(expected = RuntimeException.class)
    public void getUserWithPlaylists_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getUserWithPlayists("test"))
                .thenThrow(RuntimeException.class);

        //Act
        service.getUserWithPlaylists("test");
    }

    @Test
    public void createUserDTO_Should_ReturnUserDTO_When_Called() {
        //Arrange
        user.setRoles(new HashSet<>());
        Country country = new Country();
        country.setId(33);
        country.setName("Bulgaria");
        user.setCountry(country);
        role.setAuthority("ROLE_USER");
        user.addRole(role);
        Role role2 = new Role();
        role2.setAuthority("ROLE_ADMIN");
        user.addRole(role2);
        Mockito.when(mockRepository.getUserWithRoles("username"))
                .thenReturn(user);

        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("username");
        user.getAuthorities().forEach(role -> userDTO.addRole(role.getAuthority()));

        //Act
        UserDTO test = service.createUserDTO("username");

        //Assert
        Assert.assertEquals(userDTO.getUsername(), test.getUsername());
        Assert.assertEquals(country.getId(), test.getCountryId());
        Assert.assertEquals(userDTO.getEmail(), test.getEmail());
        Assert.assertEquals(userDTO.getRoles(), test.getRoles());
        Assert.assertEquals(userDTO.getRoles().get(0), test.getRoles().get(0));
        Assert.assertEquals(userDTO.getRoles().get(1), test.getRoles().get(1));
    }

    @Test
    public void createUserFromDTO_Should_UpdateUser_When_Called() {
        //Arrange
        UserDTO userDTO = createUserFromDTOSetup();
        Mockito.when(mockRepository.getRoleByName("ROLE_USER"))
                .thenReturn(role);

        //Act
        service.createUserFromDTO(userDTO);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).createUser(any(User.class));
    }

    @Test(expected = RuntimeException.class)
    public void createUserFromDTO_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        UserDTO userDTO = createUserFromDTOSetup();
        Mockito.when(mockRepository.getRoleByName("ROLE_USER"))
                .thenReturn(role);
        Mockito.doThrow(new RuntimeException()).when(mockRepository).createUser(any(User.class));


        //Act
        service.createUserFromDTO(userDTO);
    }

    @Test
    public void updateUserFromDTO_Should_UpdateUser_When_Called() {
        //Arrange
        UserDTO userDTO = new UserDTO();
        Mockito.when(mockRepository.getUserWithRoles("username"))
                .thenReturn(user);
        Mockito.when(mockRepository.getRoleByName("role"))
                .thenReturn(role);
        userDTO.setUsername("username");
        userDTO.setEmail("email");
        userDTO.addRole("role");

        //Act
        service.updateUserFromDTO(userDTO);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).updateUser(user);
        Mockito.verify(mockRepository, Mockito.times(1)).getRoleByName("role");
    }

    @Test(expected = RuntimeException.class)
    public void updateUser_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getUserWithRoles("user"))
                .thenReturn(user);
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("user");
        Mockito.doThrow(new RuntimeException()).when(mockRepository).updateUser(user);

        //Act
        service.updateUserFromDTO(userDTO);
    }

    @Test
    public void removeUser_Should_RemoveUser_When_Called() {
        //Arrange
        Mockito.when(mockRepository.getUser("user"))
                .thenReturn(user);

        //Act
        service.removeUser("user");

        //Assert
        Assert.assertFalse(user.isEnabled());
        Mockito.verify(mockRepository, Mockito.times(1)).updateUser(user);
    }

    @Test
    public void restoreUser_Should_RestoreUser_When_Called() {
        //Arrange
        user.setEnabled(false);
        Assert.assertFalse(user.isEnabled());
        Mockito.when(mockRepository.getDeletedUser("user"))
                .thenReturn(user);

        //Act
        service.restoreUser("user");

        //Assert
        Assert.assertTrue(user.isEnabled());
        Mockito.verify(mockRepository, Mockito.times(1)).updateUser(user);
    }

    @Test(expected = RuntimeException.class)
    public void restoreUser_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockRepository).getDeletedUser("user");

        //Act
        service.restoreUser("user");
    }

    @Test
    public void getAllRoles_Should_ReturnListWithRoles_When_Called() {
        //Arrange
        List<Role> roles = new ArrayList<>();
        Role role1 = new Role();
        role1.setAuthority("role1");
        Role role2 = new Role();
        role2.setAuthority("role2");
        Role role3 = new Role();
        role3.setAuthority("role3");
        Role role4 = new Role();
        role4.setAuthority("role4");

        roles.add(role1);
        roles.add(role2);
        roles.add(role3);
        roles.add(role4);

        Mockito.when(mockRepository.getAllRoles())
                .thenReturn(roles);

        //Act
        List<Role> result = service.getAllRoles();

        //Assert
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(roles, result);
        Assert.assertEquals(roles.get(0).getAuthority(), result.get(0).getAuthority());
        Assert.assertEquals(roles.get(1).getAuthority(), result.get(1).getAuthority());
        Assert.assertEquals(roles.get(2).getAuthority(), result.get(2).getAuthority());
        Assert.assertEquals(roles.get(3).getAuthority(), result.get(3).getAuthority());
    }

    @Test(expected = EmptyDatabaseException.class)
    public void getAllRoles_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Mockito.when(mockRepository.getAllRoles())
                .thenReturn(null);
        //Act
        service.getAllRoles();
    }

    @Test(expected = RuntimeException.class)
    public void getAllRoles_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getAllRoles())
                .thenThrow(RuntimeException.class);

        //Act
        service.getAllRoles();
    }

    @Test(expected = IllegalArgumentException.class)
    public void loadUserByUsername_Should_Throw_When_SuchUserDoesntExist() {
        //Act
        service.loadUserByUsername("test");
    }

    @Test
    public void loadUserByUsername_Should_ReturnUserDetailsObject_When_Called() {
        //Arrange
        Mockito.when(mockRepository.getUserWithRoles("user"))
                .thenReturn(user);

        //Act
        UserDetails workingUserDetails = service.loadUserByUsername("user");

        //Assert
        Assert.assertEquals(user, workingUserDetails);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getRoleByName_Should_Throw_When_SuchRoleDoesntExist() {
        //Arrange
        UserDTO userDTO = createUserFromDTOSetup();
        Mockito.when(mockRepository.getRoleByName("ROLE_USER"))
                .thenReturn(null);

        //Act
        service.createUserFromDTO(userDTO);
    }

    @Test(expected = RuntimeException.class)
    public void getRoleByName_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        UserDTO userDTO = createUserFromDTOSetup();
        Mockito.when(mockRepository.getRoleByName("ROLE_USER"))
                .thenThrow(RuntimeException.class);

        //Act
        service.createUserFromDTO(userDTO);
    }

    @Test
    public void getAllCountries_Should_ReturnListWithCountries_When_Called() {
        //Arrange
        List<Country> list = new ArrayList<>();
        Country country1 = new Country();
        country1.setId(1);
        country1.setName("1");

        Country country2 = new Country();
        country2.setId(2);
        country2.setName("2");

        list.add(country1);
        list.add(country2);

        Mockito.when(mockRepository.getAllCountries())
                .thenReturn(list);

        //Act
        List<Country> result = service.getAllCountries();

        //Assert
        Assert.assertEquals(list, result);
        Assert.assertEquals(list.get(0).getId(), result.get(0).getId());
        Assert.assertEquals(list.get(1).getId(), result.get(1).getId());
        Assert.assertEquals(list.get(0).getName(), result.get(0).getName());
        Assert.assertEquals(list.get(1).getName(), result.get(1).getName());
    }

    @Test(expected = EmptyDatabaseException.class)
    public void getAllCountries_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Mockito.when(mockRepository.getAllCountries())
                .thenReturn(null);

        //Act
        service.getAllCountries();
    }

    @Test(expected = RuntimeException.class)
    public void getAllCountries_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.when(mockRepository.getAllCountries())
                .thenThrow(RuntimeException.class);

        //Act
        service.getAllCountries();
    }

    private List<User> createUsers() {
        List<User> users = new ArrayList<>();
        User user1 = new User();
        user1.setUsername("user1");
        User user2 = new User();
        user2.setUsername("user2");
        User user3 = new User();
        user3.setUsername("user3");
        User user4 = new User();
        user4.setUsername("user4");

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        return users;
    }

    private void testsWithUsers(List<User> users, List<User> result) {
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(users, result);
        Assert.assertEquals(users.get(0).getUsername(), result.get(0).getUsername());
        Assert.assertEquals(users.get(1).getUsername(), result.get(1).getUsername());
        Assert.assertEquals(users.get(2).getUsername(), result.get(2).getUsername());
        Assert.assertEquals(users.get(3).getUsername(), result.get(3).getUsername());
    }

    private UserDTO createUserFromDTOSetup() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("username");
        userDTO.setPassword("password");
        userDTO.setEmail("email");
        userDTO.setCountryId(1);
        Country country = new Country();
        country.setId(1);
        Mockito.when(mockRepository.getCountry(1))
                .thenReturn(country);

        return userDTO;
    }
}

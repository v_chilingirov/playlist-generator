package com.playlistgenerator;

import com.playlistgenerator.exceptions.EmptyDatabaseException;
import com.playlistgenerator.models.*;
import com.playlistgenerator.repositories.contracts.PlaylistRepository;
import com.playlistgenerator.services.PlaylistServiceImpl;
import com.playlistgenerator.services.contracts.PictureService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistServiceImplTests {

    @Mock
    PlaylistRepository mockPlaylistRepository;

    @Mock
    PictureService mockPictureService;

    @InjectMocks
    PlaylistServiceImpl service;

    private Playlist playlist = new Playlist();
    private PlaylistDTO playlistDTO = new PlaylistDTO();
    private Genre genreRock = new Genre();
    private Genre genreRnB = new Genre();
    private Genre genreDance = new Genre();

    @Test
    public void getPlaylist_Should_ReturnPlaylist_When_Called() {
        //Arrange
        Mockito.when(mockPlaylistRepository.getPlaylist(1))
                .thenReturn(playlist);

        //Act
        Playlist result = service.getPlaylist(1);

        //Assert
        Assert.assertEquals(playlist, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPlaylist_Should_Throw_When_SuchPlaylistDoesntExist() {
        //Act
        service.getPlaylist(1);
    }

    @Test(expected = RuntimeException.class)
    public void getPlaylist_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getPlaylist(1);

        //Act
        service.getPlaylist(1);
    }

    @Test
    public void getPlaylistWithTracks_Should_ReturnPlaylist_When_Called() {
        //Arrange
        Mockito.when(mockPlaylistRepository.getPlaylistWithTracks(1))
                .thenReturn(playlist);

        //Act
        Playlist result = service.getPlaylistWithTracks(1);

        //Assert
        Assert.assertEquals(playlist, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPlaylistWithTracks_Should_Throw_When_SuchPlaylistDoesntExist() {
        //Act
        service.getPlaylistWithTracks(1);
    }

    @Test(expected = RuntimeException.class)
    public void getPlaylistWithTracks_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getPlaylistWithTracks(1);

        //Act
        service.getPlaylistWithTracks(1);
    }

    @Test
    public void getAllPlaylists_Should_ReturnListWithPlaylists_When_Called() {
        //Arrange
        List<Playlist> playlists = generatePlaylists();
        Mockito.when(mockPlaylistRepository.getAllPlaylists())
                .thenReturn(playlists);

        //Act
        List<Playlist> result = service.getAllPlaylists();

        //Assert
        testsWithPlaylists(playlists, result);
    }

    @Test(expected = EmptyDatabaseException.class)
    public void getAllPlaylists_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Mockito.when(mockPlaylistRepository.getAllPlaylists())
                .thenReturn(null);

        //Act
        service.getAllPlaylists();
    }

    @Test(expected = RuntimeException.class)
    public void getAllPlaylists_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getAllPlaylists();

        //Act
        service.getAllPlaylists();
    }

    @Test
    public void searchPlaylists_Should_ReturnListWithPlaylists_When_CalledWithMultipleWordsAndGenres() {
        //Arrange
        List<Playlist> playlists = generatePlaylists();
        List<String> words = new ArrayList<>();
        words.add("two");
        words.add("% two %");
        words.add("% two");
        words.add("two %");
        words.add("words");
        words.add("% words %");
        words.add("% words");
        words.add("words %");
        List<String> genresArr = new ArrayList<>();
        genresArr.add("Rock");
        genresArr.add("Dance");
        genresArr.add("R&B");
        String hql = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g WHERE (p.title LIKE :title0 OR p.title LIKE :title1 OR p.title LIKE :title2 OR p.title LIKE :title3) AND (p.title LIKE :title4 OR p.title LIKE :title5 OR p.title LIKE :title6 OR p.title LIKE :title7) AND (g.name LIKE :genre0 OR g.name LIKE :genre1 OR g.name LIKE :genre2 ) AND p.duration >= :min_duration AND p.duration <= :max_duration ORDER BY p.rank DESC";
        Mockito.when(mockPlaylistRepository.searchPlaylists(hql, words, genresArr, 0, 1800))
                .thenReturn(playlists);

        //Act
        List<Playlist> result = service.searchPlaylists("two words", "Rock Dance R&B", "0", "30");

        //Assert
        testsWithPlaylists(playlists, result);
    }

    @Test
    public void searchPlaylists_Should_ReturnListWithPlaylists_When_CalledWithoutTitle() {
        //Arrange
        List<Playlist> playlists = generatePlaylists();
        List<String> words = new ArrayList<>();
        List<String> genresArr = new ArrayList<>();
        genresArr.add("Rock");
        genresArr.add("Dance");
        genresArr.add("R&B");
        String hql = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g WHERE (g.name LIKE :genre0 OR g.name LIKE :genre1 OR g.name LIKE :genre2 ) AND p.duration >= :min_duration AND p.duration <= :max_duration ORDER BY p.rank DESC";
        Mockito.when(mockPlaylistRepository.searchPlaylists(hql, words, genresArr, 0, 1800))
                .thenReturn(playlists);

        //Act
        List<Playlist> result = service.searchPlaylists("", "Rock Dance R&B", "0", "30");

        //Assert
        testsWithPlaylists(playlists, result);
    }

    @Test
    public void searchPlaylists_Should_ReturnListWithPlaylists_When_CalledWithoutTitleAndGenres() {
        //Arrange
        List<Playlist> playlists = generatePlaylists();
        List<String> words = new ArrayList<>();
        List<String> genresArr = new ArrayList<>();
        String hql = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g WHERE p.duration >= :min_duration AND p.duration <= :max_duration ORDER BY p.rank DESC";
        Mockito.when(mockPlaylistRepository.searchPlaylists(hql, words, genresArr, 0, 1800))
                .thenReturn(playlists);

        //Act
        List<Playlist> result = service.searchPlaylists("", "", "0", "30");

        //Assert
        testsWithPlaylists(playlists, result);
    }

    @Test
    public void searchPlaylists_Should_ReturnListWithPlaylists_When_CalledWithoutTitleAndGenresAndMinDuration() {
        //Arrange
        List<Playlist> playlists = generatePlaylists();
        List<String> words = new ArrayList<>();
        List<String> genresArr = new ArrayList<>();
        String hql = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g WHERE p.duration <= :max_duration ORDER BY p.rank DESC";
        Mockito.when(mockPlaylistRepository.searchPlaylists(hql, words, genresArr, 0, 1800))
                .thenReturn(playlists);

        //Act
        List<Playlist> result = service.searchPlaylists("", "", "", "30");

        //Assert
        testsWithPlaylists(playlists, result);
    }

    @Test(expected = RuntimeException.class)
    public void searchPlaylists_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        List<String> words = new ArrayList<>();
        List<String> genresArr = new ArrayList<>();
        String hql = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g WHERE p.duration <= :max_duration ORDER BY p.rank DESC";
        Mockito.when(mockPlaylistRepository.searchPlaylists(hql, words, genresArr, 0, 1800))
                .thenThrow(RuntimeException.class);

        //Act
        service.searchPlaylists("", "", "", "30");
    }

    @Test
    public void getAllGenres_Should_ReturnListWithGenres_When_Called() {
        //Arrange
        List<Genre> genres = generateGenres();
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(genres);

        //Act
        List<Genre> result = service.getAllGenres();

        //Assert
        testsWithGenres(genres, result);
    }

    @Test(expected = EmptyDatabaseException.class)
    public void getAllGenres_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(null);

        //Act
        service.getAllGenres();
    }

    @Test(expected = RuntimeException.class)
    public void getAllGenres_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getAllGenres();

        //Act
        service.getAllGenres();
    }

    @Test
    public void convertDTOToNewPlaylist_Should_ReturnPlaylist_When_Called() {
        //Arrange
        User user = new User();
        setPlaylistDTO();
        generateListStringGenres();
        user.setUsername("username");
        initGenres();
        Mockito.when(mockPlaylistRepository.getGenreByName("Rock"))
                .thenReturn(genreRock);
        Mockito.when(mockPlaylistRepository.getGenreByName("R&B"))
                .thenReturn(genreRnB);
        Mockito.when(mockPlaylistRepository.getGenreByName("Dance"))
                .thenReturn(genreDance);
        Mockito.when(mockPictureService.getRandomPicture())
                .thenReturn("https://cdn.pixabay.com/photo/2018/05/31/23/57/cranium-3445434_150.png");

        //Act
        Playlist result = service.convertDTOToNewPlaylist(playlistDTO, user);

        //Assert
        Assert.assertEquals(playlistDTO.getTitle(), result.getTitle());
        Assert.assertEquals(playlistDTO.getGenres().size(), result.getGenres().size());
        Assert.assertTrue(result.getGenres().contains(genreRock));
        Assert.assertTrue(result.getGenres().contains(genreRnB));
        Assert.assertTrue(result.getGenres().contains(genreDance));
        Assert.assertTrue(result.getPicture().length != 0);
        Assert.assertEquals(user, result.getUser());
        Assert.assertEquals(user.getUsername(), result.getUser().getUsername());
    }

    @Test
    public void addPictureToPlaylist_Should_SetPictureToNull_When_CalledWithNonExistantURL() {
        //Arrange
        playlist.setPicture(new byte[5]);
        Assert.assertNotNull(playlist.getPicture());
        Mockito.when(mockPictureService.getRandomPicture())
                .thenReturn("test");

        //Act
        service.addPictureToPlaylist(playlist);

        //Assert
        Assert.assertNull(playlist.getPicture());
    }

    @Test
    public void calculatingGenresDuration_Should_ReturnMapWithDurationForEachGenre_When_CalledWithOneGenre() {
        //Arrange
        List<Genre> list = new ArrayList<>();
        list.add(genreRock);
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(list);
        List<Integer> percentages = new ArrayList<>();
        percentages.add(100);
        playlistDTO.setPercentages(percentages);
        //1h and 20 minutes
        playlistDTO.setDuration(80);

        //Act
        Map<Genre, Integer> result = service.calculatingGenresDuration(playlistDTO);

        //Assert
        Assert.assertEquals(Integer.valueOf(4800), result.get(genreRock));
    }

    @Test
    public void calculatingGenresDuration_Should_ReturnMapWithDurationForEachGenre_When_CalledWithTwoGenres() {
        //Arrange
        initGenres();
        List<Genre> list = new ArrayList<>();
        list.add(genreRock);
        list.add(genreRnB);
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(list);
        List<Integer> percentages = new ArrayList<>();
        percentages.add(60);
        percentages.add(40);
        playlistDTO.setPercentages(percentages);
        //11 minutes
        playlistDTO.setDuration(11);

        //Act
        Map<Genre, Integer> result = service.calculatingGenresDuration(playlistDTO);

        //Assert
        Assert.assertEquals(Integer.valueOf(396), result.get(genreRock));
        Assert.assertEquals(Integer.valueOf(264), result.get(genreRnB));
    }

    @Test
    public void calculatingGenresDuration_Should_ReturnMapWithDurationForEachGenre_When_CalledWithThreeGenres() {
        //Arrange
        initGenres();
        List<Genre> list = new ArrayList<>();
        list.add(genreRock);
        list.add(genreRnB);
        list.add(genreDance);
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(list);
        List<Integer> percentages = new ArrayList<>();
        percentages.add(33);
        percentages.add(33);
        percentages.add(33);
        playlistDTO.setPercentages(percentages);
        //30 minutes
        playlistDTO.setDuration(30);

        //Act
        Map<Genre, Integer> result = service.calculatingGenresDuration(playlistDTO);

        //Assert
        Assert.assertEquals(Integer.valueOf(594), result.get(genreRock));
        Assert.assertEquals(Integer.valueOf(594), result.get(genreRnB));
        Assert.assertEquals(Integer.valueOf(594), result.get(genreDance));
    }

    @Test
    public void generateRandomTracks_Should_ReturnSetOfTracks_When_CalledWithBothBooleansAsTrue() {
        //Arrange
        Set<Artist> selectedArtists = new HashSet<>();
        List<Track> list = createTracks();
        Mockito.when(mockPlaylistRepository.getTopTracksFromGenre(genreRock))
                .thenReturn(list);
        Map<Genre, Integer> genresDuration = new HashMap<>();
        genresDuration.put(genreRock, 1000);

        //Act
        Set<Track> result = service.generateRandomTracks(genresDuration, selectedArtists,
                true, true);

        //Assert
        Assert.assertEquals(3, result.size());
        Assert.assertTrue(result.contains(list.get(0)));
        Assert.assertTrue(result.contains(list.get(1)));
        Assert.assertTrue(result.contains(list.get(2)));
    }

    @Test
    public void generateRandomTracks_Should_ReturnSetOfTracks_When_CalledWithBothBooleansAsFalse() {
        //Arrange
        Set<Artist> selectedArtists = new HashSet<>();
        List<Track> list = createTracks();
        list.get(2).getAlbum().setArtist(list.get(1).getAlbum().getArtist());
        list.remove(4);
        list.remove(3);
        Mockito.when(mockPlaylistRepository.getAllTracksFromGenre(genreRock))
                .thenReturn(list);
        Map<Genre, Integer> genresDuration = new HashMap<>();
        genresDuration.put(genreRock, 1000);

        //Act
        Set<Track> result = service.generateRandomTracks(genresDuration, selectedArtists,
                false, false);

        //Assert
        Assert.assertEquals(2, result.size());
    }

    @Test(expected = EmptyDatabaseException.class)
    public void generateRandomTracks_Should_Throw_When_DatabaseIsEmpty() {
        //Arrange
        Set<Artist> selectedArtists = new HashSet<>();
        Mockito.when(mockPlaylistRepository.getTopTracksFromGenre(genreRock))
                .thenReturn(null);
        Map<Genre, Integer> genresDuration = new HashMap<>();
        genresDuration.put(genreRock, 1000);

        //Act
        Set<Track> result = service.generateRandomTracks(genresDuration, selectedArtists,
                false, true);
    }

    @Test(expected = RuntimeException.class)
    public void generateRandomTracks_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Set<Artist> selectedArtists = new HashSet<>();
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getTopTracksFromGenre(genreRock);
        Map<Genre, Integer> genresDuration = new HashMap<>();
        genresDuration.put(genreRock, 1000);

        //Act
        service.generateRandomTracks(genresDuration, selectedArtists, false, true);
    }

    @Test
    public void addTracksToPlaylist_Should_AddThemSuccessfully_When_Called() {
        //Arrange
        Set<Track> tracks = new HashSet<>(createTracks());

        //Act
        service.addTracksToPlaylist(playlist, tracks);

        //Assert
        Assert.assertEquals(5, playlist.getTracks().size());
        Assert.assertTrue(!playlist.getTracks().isEmpty());
    }

    @Test
    public void createPlaylist_Should_CreatePlaylistFromDTO_When_Called() {
        //Arrange
        List<Integer> percentages = new ArrayList<>();
        percentages.add(100);
        playlistDTO.setPercentages(percentages);
        playlistDTO.setDuration(660);
        playlistDTO.setRepetitionAllowed(true);
        playlistDTO.setTopTracksOnly(true);
        List<String> genres = new ArrayList<>();
        genres.add("Rock");
        playlistDTO.setGenres(genres);
        List<Genre> list = new ArrayList<>();
        list.add(genreRock);
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(list);
        Mockito.when(mockPlaylistRepository.getGenreByName("Rock"))
                .thenReturn(genreRock);


        //Act
        service.createPlaylist(playlistDTO, new User());

        //Assert
        Mockito.verify(mockPlaylistRepository, Mockito.times(1)).getGenreByName(any(String.class));
        Mockito.verify(mockPictureService, Mockito.times(1)).getRandomPicture();
        Mockito.verify(mockPlaylistRepository, Mockito.times(1)).getAllGenres();
        Mockito.verify(mockPlaylistRepository, Mockito.times(1)).createPlaylist(any(Playlist.class));
    }

    @Test
    public void updatePlaylistFromDTO_Should_UpdateRealPlaylist_When_Called() {
        //Arrange
        setPlaylistDTO();
        Mockito.when(mockPlaylistRepository.getPlaylist(1))
                .thenReturn(playlist);
        generateListStringGenres();

        //Act
        service.updatePlaylistFromDTO(playlistDTO);

        //Assert
        Mockito.verify(mockPlaylistRepository, Mockito.times(1)).updatePlaylist(any(Playlist.class));
    }

    @Test(expected = RuntimeException.class)
    public void updatePlaylistFromDTO_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        playlistDTO.setId(1);
        Mockito.when(mockPlaylistRepository.getPlaylist(1))
                .thenReturn(playlist);
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).updatePlaylist(playlist);


        //Act
        service.updatePlaylistFromDTO(playlistDTO);
    }

    @Test(expected = RuntimeException.class)
    public void addPlaylistToDatabase_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        List<Integer> percentages = new ArrayList<>();
        percentages.add(100);
        playlistDTO.setPercentages(percentages);
        playlistDTO.setDuration(660);
        playlistDTO.setRepetitionAllowed(true);
        playlistDTO.setTopTracksOnly(true);
        List<String> genres = new ArrayList<>();
        genres.add("Rock");
        playlistDTO.setGenres(genres);
        List<Genre> list = new ArrayList<>();
        list.add(genreRock);
        Mockito.when(mockPlaylistRepository.getAllGenres())
                .thenReturn(list);
        Mockito.when(mockPlaylistRepository.getGenreByName("Rock"))
                .thenReturn(genreRock);
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).createPlaylist(any(Playlist.class));


        //Act
        service.createPlaylist(playlistDTO, new User());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deletePlaylist_Should_Throw_When_SuchPlaylistDoesntExist() {
        //Act
        service.deletePlaylist(1);
    }

    @Test(expected = RuntimeException.class)
    public void deletePlaylist_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).deletePlaylist(playlist);
        Mockito.when(mockPlaylistRepository.getPlaylist(1))
                .thenReturn(playlist);

        //Act
        service.deletePlaylist(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getGenreByName_Should_Throw_When_SuchGenreDoesntExist() {
        //Arrange
        generateListStringGenres();

        //Act
        Playlist result = service.convertDTOToNewPlaylist(playlistDTO, new User());
    }

    @Test(expected = RuntimeException.class)
    public void getGenreByName_Should_Throw_When_ThereIsAProblemWithTheDatabase() {
        //Arrange
        generateListStringGenres();
        Mockito.doThrow(new RuntimeException()).when(mockPlaylistRepository).getGenreByName("Rock");

        //Act
        Playlist result = service.convertDTOToNewPlaylist(playlistDTO, new User());
    }

    private List<Playlist> generatePlaylists() {
        List<Playlist> list = new ArrayList<>();

        Playlist playlist1 = new Playlist();
        playlist1.setId(1);
        Playlist playlist2 = new Playlist();
        playlist2.setId(2);
        Playlist playlist3 = new Playlist();
        playlist3.setId(3);
        Playlist playlist4 = new Playlist();
        playlist4.setId(4);

        list.add(playlist1);
        list.add(playlist2);
        list.add(playlist3);
        list.add(playlist4);

        return list;
    }

    private void testsWithPlaylists(List<Playlist> playlists, List<Playlist> result) {
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(playlists, result);
        Assert.assertEquals(playlists.get(0).getId(), result.get(0).getId());
        Assert.assertEquals(playlists.get(1).getId(), result.get(1).getId());
        Assert.assertEquals(playlists.get(2).getId(), result.get(2).getId());
        Assert.assertEquals(playlists.get(3).getId(), result.get(3).getId());
    }

    private List<Genre> generateGenres() {
        List<Genre> list = new ArrayList<>();

        Genre genre1 = new Genre();
        genre1.setGenreId(1);
        Genre genre2 = new Genre();
        genre2.setGenreId(2);
        Genre genre3 = new Genre();
        genre3.setGenreId(3);
        Genre genre4 = new Genre();
        genre4.setGenreId(4);

        list.add(genre1);
        list.add(genre2);
        list.add(genre3);
        list.add(genre4);

        return list;
    }

    private void testsWithGenres(List<Genre> genres, List<Genre> result) {
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(genres, result);
        Assert.assertEquals(genres.get(0).getGenreId(), result.get(0).getGenreId());
        Assert.assertEquals(genres.get(1).getGenreId(), result.get(1).getGenreId());
        Assert.assertEquals(genres.get(2).getGenreId(), result.get(2).getGenreId());
        Assert.assertEquals(genres.get(3).getGenreId(), result.get(3).getGenreId());
    }

    private void setPlaylistDTO() {
        playlistDTO.setId(1);
        playlistDTO.setTitle("test");
    }

    private void generateListStringGenres() {
        List<String> genres = new ArrayList<>();
        genres.add("Rock");
        genres.add("R&B");
        genres.add("Dance");
        playlistDTO.setGenres(genres);
    }

    private List<Track> createTracks() {
        List<Track> list = new ArrayList<>();

        Track track1 = new Track();
        track1.setDuration(300);
        Album album1 = new Album();
        Artist artist1 = new Artist();
        artist1.setId(1);
        album1.setArtist(artist1);
        track1.setAlbum(album1);
        track1.setId(1);

        Track track2 = new Track();
        track2.setDuration(300);
        Album album2 = new Album();
        Artist artist2 = new Artist();
        artist2.setId(2);
        album2.setArtist(artist2);
        track2.setAlbum(album2);
        track2.setId(2);


        Track track3 = new Track();
        track3.setDuration(300);
        Album album3 = new Album();
        Artist artist3 = new Artist();
        artist3.setId(3);
        album3.setArtist(artist3);
        track3.setAlbum(album3);
        track3.setId(3);

        Track track4 = new Track();
        track4.setDuration(300);
        Album album4 = new Album();
        Artist artist4 = new Artist();
        artist4.setId(4);
        album4.setArtist(artist4);
        track4.setAlbum(album4);
        track4.setId(4);

        Track track5 = new Track();
        track5.setDuration(300);
        Album album5 = new Album();
        Artist artist5 = new Artist();
        artist5.setId(5);
        album5.setArtist(artist5);
        track5.setAlbum(album5);
        track5.setId(5);

        list.add(track1);
        list.add(track2);
        list.add(track3);
        list.add(track4);
        list.add(track5);
        return list;
    }

    private void initGenres() {
        genreRock.setName("Rock");
        genreRnB.setName("R&B");
        genreDance.setName("Dance");
    }
}

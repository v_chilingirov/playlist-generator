"use strict";

// $(document).ready(function () {
//     const genresPercentages = $('.genres-percentages').find("input");
//     const numberOfGenres = genresPercentages.length;
//     const weight = parseInt(100 / numberOfGenres, 10);
//     let remainder = 100 - weight * numberOfGenres;
//     const partOfRemainder = 1;
//     genresPercentages.each(function (index) {
//         if (remainder > 0) {
//             genresPercentages.eq(index).val(weight + partOfRemainder);
//             remainder--;
//         } else {
//             genresPercentages.eq(index).val(weight);
//         }
//     })
// });


$(document).ready(generateValues());

function generateValues() {
    const genresPercentages = $('.genres-percentages').find("input");
    const numberOfGenres = genresPercentages.length;
    const weight = parseInt(100 / numberOfGenres, 10);
    let remainder = 100 - weight * numberOfGenres;
    const partOfRemainder = 1;
    genresPercentages.each(function (index) {
        if (remainder > 0) {
            genresPercentages.eq(index).val(weight + partOfRemainder);
            remainder--;
        } else {
            genresPercentages.eq(index).val(weight);
        }
    })
}

$(document).on("submit", ".create-playlist", function (e) {
    const percentages = $('.genres-percentages').find("input");
    let sum = 0;
    percentages.each(function (index) {
        if (percentages.eq(index).val() != 0) {
            sum += parseInt(percentages.eq(index).val());
        }
    });
    if (sum !== 100) {
        alert("Genres should sum up to 100%");
        e.preventDefault();
    } else {
        if ($(".genres").find(":checkbox:checked").length === 0) {
            const genres = $(".genres").find(":checkbox");
            genres.each(function (index, elem) {
                genres.eq(index).prop('checked', true);
            });
        }
    }
});

function showActiveGenres() {
    const genres = $(".genres").find(":checkbox");
    const genresPercentages = $('.genres-percentages');
    const activeGenres = $(".genres").find(":checkbox:checked").length;
    const weight = parseInt(100 / activeGenres, 10);
    let remainder = 100 - weight * activeGenres;
    const partOfRemainder = 1;
    genres.each(function (index, elem) {
        const genreP = genresPercentages.eq(index);
        if (elem.checked) {
            if (remainder > 0) {
                genreP.find("input").val(weight + partOfRemainder);
                remainder--;
            } else {
                genreP.find("input").val(weight);
                genreP.eq(index).show();
            }
            genreP.show();
        } else {
            genreP.find("input").val(0);
            genreP.hide();
        }
    });

    if (activeGenres === 0) {
        generateValues();
    }
}

function toggleVisibility() {
    const options = $(".more-options");
    if (options.is(":visible")) {
        options.hide();
        $("#opt-btn").html('More');
    } else {
        options.show();
        $("#opt-btn").html('Less');
    }
}

function goToTravel() {
    location.href = "/journey";
}


function goToSearch() {
    let title = "";
    if ($("#title").val() !== "") {
        title = "title=" + $("#title").val().replace(/&/g, "%26")
            .replace(/\?/g, "%3F")
            .replace(/\//g, "%2F")
            .replace(/=/g, "%3D")
            .replace(/%/g, "%25");
    }

    const genres = $("input:checkbox:checked");
    let activeGenres = "";
    const totalGenres = $("input:checkbox").length;
    if (genres.length !== totalGenres && genres.length !== 0) {
        if (title !== "") {
            activeGenres += "&";
        }
        activeGenres += "genres=";
        genres.each(function (index) {
            let genre = genres.eq(index).val().replace(/&/g, "%26");
            activeGenres += genre + " ";
        })
    }

    let minDurationValue = $("#min-duration").val();
    let maxDurationValue = $("#max-duration").val();

    let minDuration = "";
    let maxDuration = "";

    if (minDurationValue > maxDurationValue) {
        let transferValue = minDurationValue;
        minDurationValue = maxDurationValue;
        maxDurationValue = transferValue;
    }

    const maxDurationValueToShow = 600;

    if ((minDurationValue != 0 && minDurationValue != maxDurationValueToShow) ||
        (maxDurationValue != 0 && maxDurationValue != maxDurationValueToShow)) {
        if (title !== "" || activeGenres !== "") {
            minDuration += "&";
        }
        minDuration += "min-duration=" + minDurationValue;
        maxDuration += "&max-duration=" + maxDurationValue;
    }

    window.location = "/playlists?" + title + activeGenres + minDuration + maxDuration;
}


$('#title').on('keypress', (event) => {
    if (event.which === 13) {
        event.preventDefault();
        goToSearch();
    }
});

$(document).ready(function () {
    const elements = $(".duration-as-time");

    elements.each(function (index) {
        const seconds = parseInt(elements.eq(index).text());
        const hours = parseInt(seconds / 60 / 60);
        const minutes = parseInt((seconds - hours * 60 * 60) / 60);

        let output = "";
        if (hours !== 0) {
            output += hours.toString() + " hours";
        }
        if (minutes !== 0) {
            if (hours !== 0) {
                output += " and "
            }
            output += minutes.toString() + " minutes";

        }

        elements.eq(index).html(output);
    })
})

$(document).ready(function () {
    const elements = $(".duration-track");

    elements.each(function (index) {
        let seconds = parseInt(elements.eq(index).text());
        const hours = parseInt(seconds / 60 / 60);
        const minutes = parseInt((seconds - hours * 60 * 60) / 60);
        seconds = seconds - hours * 60 * 60 - minutes * 60;

        let output = "";

        if (minutes < 10) {
            output += "0" + minutes + ":";
        } else {
            output += minutes + ":";
        }

        if (seconds < 10) {
            output += "0" + seconds;
        } else {
            output += seconds;
        }

        elements.eq(index).html(output);
    })
})

$(document).ready(function () {
    const element = $(".duration-format");

    let minutes = parseInt(element.text());
    const hours = parseInt(minutes / 60);
    minutes = minutes - hours * 60;

    let output = "";
    if (hours !== 0) {
        output += hours.toString() + " hours";
    }
    if (minutes !== 0) {
        if (hours !== 0) {
            output += " and "
        }
        output += minutes.toString() + " minutes";
    }

    element.html(output);
})


function goToPlaylistList() {
    location.href = "/admin/playlists";
}

function goToMyPlaylists() {
    location.href = "/profile/my-playlists";
}

function goToRegistration() {
    location.href = "/registration";
}

function goToUsers() {
    location.href = "/admin/users";
}

function goToSync() {
    location.href = "/admin/sync";
}
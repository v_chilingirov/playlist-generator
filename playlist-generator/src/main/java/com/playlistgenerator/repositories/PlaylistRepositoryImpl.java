package com.playlistgenerator.repositories;

import com.playlistgenerator.models.Genre;
import com.playlistgenerator.models.GenreList;
import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.Track;
import com.playlistgenerator.repositories.contracts.PlaylistRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class PlaylistRepositoryImpl implements PlaylistRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlaylistRepositoryImpl.class);
    private static final String exceptionMsg = "Exception thrown from PlaylistRepositoryImpl::%s";

    private SessionFactory sessionFactory;

    @Autowired
    public PlaylistRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Playlist getPlaylist(long playlistId) {
        Playlist playlist;
        try (Session session = sessionFactory.openSession()) {
            playlist = session.get(Playlist.class, playlistId);
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getPlaylist"));
        }

        return playlist;
    }

    @Override
    public Playlist getPlaylistWithTracks(long playlistId) {
        Playlist playlist;
        try (Session session = sessionFactory.openSession()) {
            playlist = session.createQuery("FROM Playlist p JOIN FETCH p.tracks JOIN FETCH p.user WHERE p.id = :playlist_id"
                    , Playlist.class)
                    .setParameter("playlist_id", playlistId)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getPlaylistWithTracks"));
        }

        return playlist;
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("SELECT DISTINCT p FROM Playlist p LEFT JOIN FETCH p.genres ORDER BY p.rank DESC", Playlist.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllPlaylists"));
        }
    }

    @Override
    public List<Playlist> searchPlaylists(String hql, List<String> words, List<String> genres, int minDuration, int maxDuration) {
        try (Session session = sessionFactory.openSession()) {
            org.hibernate.query.Query query = session.createQuery(hql, Playlist.class);

            for (int i = 0; i < words.size(); i++) {
                query.setParameter(String.format("title%d", i), words.get(i));
            }

            for (int i = 0; i < genres.size(); i++) {
                query.setParameter(String.format("genre%d", i), genres.get(i));
            }

            if (minDuration != 0) {
                query.setParameter("min_duration", minDuration);
            }

            if (maxDuration != 0) {
                query.setParameter("max_duration", maxDuration);
            }

            return query.list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "searchPlaylists"));
        }
    }

    @Override
    public long createPlaylist(Playlist playlist) {
        long id;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            id = (long) session.save(playlist);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "createPlaylist"));
        }
        return id;
    }

    @Override
    public void updatePlaylist(Playlist playlist) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(playlist);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "updatePlaylist"));
        }
    }

    @Override
    public void deletePlaylist(Playlist playlist) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(playlist);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "deletePlaylist"));
        }
    }

    @Override
    public List<Genre> getAllGenres() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Genre WHERE name = 'Rock' OR name = 'Dance' OR name = 'R&B'", Genre.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllGenres"));
        }
    }

    private List<Genre> getAllTheGenres() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Genre", Genre.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllTheGenres"));
        }
    }

    @Override
    public Genre getGenreByName(String genreName) {
        Genre genre;
        try (Session session = sessionFactory.openSession()) {
            genre = session.createQuery("FROM Genre WHERE name = :genre_name", Genre.class)
                    .setParameter("genre_name", genreName)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getGenreByName"));
        }

        return genre;
    }

    @Override
    public List<Track> getAllTracksFromGenre(Genre genre) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Track WHERE genre=:genre", Track.class)
                    .setParameter("genre", genre)
                    .list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllTracksFromGenre"));
        }
    }

    @Override
    public List<Track> getTopTracksFromGenre(Genre genre) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Track WHERE genre=:genre ORDER BY rank DESC ", Track.class)
                    .setParameter("genre", genre)
                    .list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getTopTracksFromGenre"));
        }
    }

    @Override
    public void syncGenres() {
        try (Session session = sessionFactory.openSession()) {
            Set<Genre> current = new HashSet<>(getAllTheGenres());
            session.beginTransaction();
            RestTemplate restTemplate = new RestTemplate();
            GenreList genreList = restTemplate.getForObject("https://api.deezer.com/genre", GenreList.class);
            if (genreList == null) {
                LOGGER.warn("Deezer returned null");
                session.getTransaction().commit();
                return;
            }

            Set<Genre> all = new HashSet<>(genreList.getData());
            for (Genre genre : all) {
                if (current.add(genre)) {
                    LOGGER.info(genre.getName() + " was added");
                    addNewGenre(genre);
                }
            }

            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "syncGenres"));
        }
    }

    private void addNewGenre(Genre genre) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(genre);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "addNewGenre"));
        }
    }
}

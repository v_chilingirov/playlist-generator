package com.playlistgenerator.repositories;

import com.playlistgenerator.models.Country;
import com.playlistgenerator.models.Role;
import com.playlistgenerator.models.User;
import com.playlistgenerator.repositories.contracts.UserRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryImpl.class);
    private static final String exceptionMsg = "Exception thrown from UserRepositoryImpl::%s";

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM User WHERE enabled = true", User.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllUsers"));
        }
    }

    @Override
    public List<User> getAllDeletedUsers() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM User WHERE enabled = false", User.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllDeletedUsers"));
        }
    }

    @Override
    public User getUser(String username) {
        User user;
        try (Session session = sessionFactory.openSession()) {
            user = session.createQuery("FROM User WHERE enabled = true AND username = :username", User.class)
                    .setParameter("username", username)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUser"));
        }
        return user;
    }

    @Override
    public User getUserWithRoles(String username) {
        User user;
        try (Session session = sessionFactory.openSession()) {
            user = session.createQuery("FROM User u JOIN FETCH u.roles " +
                    "WHERE u.enabled = true AND u.username = :username", User.class)
                    .setParameter("username", username)
                    .uniqueResult();

        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUserWithRoles"));
        }
        return user;
    }

    @Override
    public User getUserWithPlayists(String username) {
        User user;
        try (Session session = sessionFactory.openSession()) {
            user = session.createQuery("FROM User u LEFT JOIN FETCH u.playlists WHERE u.enabled = true AND u.username = :username", User.class)
                    .setParameter("username", username)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUserWithPlaylists"));
        }
        return user;
    }

    @Override
    public User getDeletedUser(String username) {
        User user;
        try (Session session = sessionFactory.openSession()) {
            user = session.createQuery("FROM User WHERE enabled = false AND username = :username", User.class)
                    .setParameter("username", username)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getDeletedUser"));
        }

        return user;
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "createUser"));
        }
    }

    @Override
    public void updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "updateUser"));
        }
    }

    @Override
    public List<Role> getAllRoles() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Role", Role.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllRoles"));
        }
    }

    @Override
    public Role getRoleByName(String roleName) {
        Role role;
        try (Session session = sessionFactory.openSession()) {
            role = session.createQuery("FROM Role WHERE authority = :role_name", Role.class)
                    .setParameter("role_name", roleName)
                    .uniqueResult();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getRoleByName"));
        }

        return role;
    }

    @Override
    public Country getCountry(long countryId) {
        Country country;
        try (Session session = sessionFactory.openSession()) {
            country = session.get(Country.class, countryId);
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getCountry"));
        }

        return country;
    }

    @Override
    public List<Country> getAllCountries() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Country ", Country.class).list();
        } catch (HibernateException he) {
            LOGGER.warn(he.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllCountries"));
        }
    }
}

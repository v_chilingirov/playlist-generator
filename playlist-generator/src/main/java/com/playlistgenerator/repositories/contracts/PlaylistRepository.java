package com.playlistgenerator.repositories.contracts;

import com.playlistgenerator.models.Genre;
import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.Track;

import java.util.List;

public interface PlaylistRepository {

    Playlist getPlaylist(long playlistId);

    Playlist getPlaylistWithTracks(long playlistId);

    List<Playlist> getAllPlaylists();

    List<Playlist> searchPlaylists(String hql, List<String> words, List<String> genres,
                                   int minDuration, int maxDuration);

    long createPlaylist(Playlist playlist);

    void updatePlaylist(Playlist playlist);

    void deletePlaylist(Playlist playlist);

    List<Genre> getAllGenres();

    Genre getGenreByName(String genreName);

    List<Track> getAllTracksFromGenre(Genre genre);

    List<Track> getTopTracksFromGenre(Genre genre);

    void syncGenres();
}

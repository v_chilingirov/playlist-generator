package com.playlistgenerator.repositories.contracts;

import com.playlistgenerator.models.Country;
import com.playlistgenerator.models.Role;
import com.playlistgenerator.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAllUsers();

    List<User> getAllDeletedUsers();

    User getUser(String username);

    User getUserWithRoles(String username);

    User getUserWithPlayists(String username);

    User getDeletedUser(String username);

    void createUser(User user);

    void updateUser(User user);

    List<Role> getAllRoles();

    Role getRoleByName(String roleName);

    Country getCountry(long countryId);

    List<Country> getAllCountries();
}

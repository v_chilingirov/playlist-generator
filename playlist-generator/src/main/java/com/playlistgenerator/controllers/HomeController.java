package com.playlistgenerator.controllers;

import com.playlistgenerator.models.Journey;
import com.playlistgenerator.models.Location;
import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.User;
import com.playlistgenerator.services.contracts.PlaylistService;
import com.playlistgenerator.services.contracts.TravelDurationService;
import com.playlistgenerator.services.contracts.UserService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
    private static final String exceptionMsg = "There was a problem with the database";

    private TravelDurationService service;
    private UserService userService;
    private PlaylistService playlistService;

    @Autowired
    HomeController(TravelDurationService service, UserService userService, PlaylistService playlistService) {
        this.service = service;
        this.userService = userService;
        this.playlistService = playlistService;
    }

    @GetMapping("/")
    public String home(Model model, HttpServletRequest request,
                       @RequestParam(value = "page", required = false) Optional<Integer> page,
                       @RequestParam(value = "size", required = false) Optional<Integer> size) {
        userService.addLoggedInUserAtribute(model, request);
        try {
            model.addAttribute("playlists", playlistService.getAllPlaylists());
            model.addAttribute("genres", playlistService.getAllGenres());
            List<Playlist> playlists = playlistService.getAllPlaylists();
            playlistService.pagination(model, page, size, playlists, playlistService);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "home";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/journey")
    public String addAddressesForm(Model model, HttpServletRequest request) {
        User user = userService.getLoggedInUser(request);
        Journey journey = new Journey();
        Location location = new Location();
        location.setCountryRegion(user.getCountry().getName());
        journey.setStartLocation(location);
        journey.setEndLocation(location);
        model.addAttribute("user", user);
        model.addAttribute("journey", journey);
        return "journey";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/journey")
    public String calculateTravelDuration(@ModelAttribute Journey journey, BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "journey";
        }

        try {
            service.calculateTravelDuration(journey);
        } catch (JSONException | IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }

        return "redirect:/create-playlist";
    }
}

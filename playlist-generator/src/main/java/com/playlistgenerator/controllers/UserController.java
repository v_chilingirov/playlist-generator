package com.playlistgenerator.controllers;

import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.PlaylistDTO;
import com.playlistgenerator.models.User;
import com.playlistgenerator.models.UserDTO;
import com.playlistgenerator.services.contracts.PlaylistService;
import com.playlistgenerator.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String exceptionMsg = "There was a problem with the database";

    private UserService userService;
    private PlaylistService playlistService;

    @Autowired
    public UserController(UserService userService, PlaylistService playlistService) {
        this.userService = userService;
        this.playlistService = playlistService;
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @GetMapping("/registration")
    public String addUserForm(Model model) {
        model.addAttribute("userDTO", new UserDTO());
        model.addAttribute("countries", userService.getAllCountries());
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@Valid @ModelAttribute UserDTO userDTO, BindingResult bindingErrors
            , HttpServletRequest request) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "registration";
        }

        try {
            userService.createUserFromDTO(userDTO);
            userService.autoLogin(request, userDTO.getUsername(), userDTO.getPassword());
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn(e.getMessage());
            return "registration-error";
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }
        return "redirect:/";
    }


    @GetMapping("/profile/my-playlists")
    @PreAuthorize("isAuthenticated()")
    public String showProfile(Model model, HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        User user = userService.getUserWithPlaylists(principal.getName());
        model.addAttribute("user", user);
        return "my-playlists";
    }

    @GetMapping("/profile/my-playlists/{id}/edit")
    @PreAuthorize("isAuthenticated()")
    public String editPlaylistForm(Model model, HttpServletRequest request, @PathVariable long id) {
        try {
            Principal principal = request.getUserPrincipal();
            User user = userService.getUserWithPlaylists(principal.getName());
            model.addAttribute("user", user);

            Playlist wantedPlaylist = user.getPlaylists()
                    .stream()
                    .filter(playlist -> playlist.getId() == id)
                    .findAny()
                    .orElse(null);
            PlaylistDTO playlistDTO = new PlaylistDTO();
            playlistDTO.setId(wantedPlaylist.getId());
            playlistDTO.setTitle(wantedPlaylist.getTitle());

            model.addAttribute("playlist", playlistDTO);

        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "edit-playlist-user";
    }

    @PostMapping("/profile/my-playlists/{id}/edit")
    @PreAuthorize("isAuthenticated()")
    public String editPlaylist(@ModelAttribute PlaylistDTO playlistDTO, BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "edit-playlist-user";
        }

        try {
            playlistService.updatePlaylistFromDTO(playlistDTO);

        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //404
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "redirect:/profile/my-playlists";
    }

    @PostMapping("/profile/my-playlists/{id}/delete")
    @PreAuthorize("isAuthenticated()")
    public String deletePlaylist(@ModelAttribute PlaylistDTO playlistDTO) {
        try {
            playlistService.deletePlaylist(playlistDTO.getId());
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "redirect:/profile/my-playlists";
    }

    @GetMapping("/profile/edit-profile")
    @PreAuthorize("isAuthenticated()")
    public String editProfileForm(Model model, HttpServletRequest request) {
        User user = userService.getLoggedInUser(request);
        model.addAttribute("userDTO", userService.createUserDTO(user.getUsername()));
        model.addAttribute("countries", userService.getAllCountries());
        return "edit-profile";
    }

    @PostMapping("/profile/edit-profile")
    @PreAuthorize("isAuthenticated()")
    public String editProfile(@ModelAttribute UserDTO userDTO, BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "edit-profile";
        }

        userService.checkForUpdateUserFromDTOThrowing(userDTO, userService, LOGGER, exceptionMsg);

        return "edit-profile";
    }


}

package com.playlistgenerator.controllers;

import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.PlaylistDTO;
import com.playlistgenerator.models.UserDTO;
import com.playlistgenerator.services.contracts.PlaylistService;
import com.playlistgenerator.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
    private static final String exceptionMsg = "There was a problem with the database";

    private UserService userService;
    private PlaylistService playlistService;

    @Autowired
    public AdminController(UserService userService, PlaylistService playlistService) {
        this.userService = userService;
        this.playlistService = playlistService;
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public String allUsers(Model model, HttpServletRequest request) {
        try {
            userService.addLoggedInUserAtribute(model, request);
            model.addAttribute("users", userService.getAllUsers());
            model.addAttribute("deletedUsers", userService.getAllDeletedUsers());
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }
        return "users";
    }

    @GetMapping("/users/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editUserForm(Model model, @PathVariable String username, HttpServletRequest request) {
        try {
            userService.addLoggedInUserAtribute(model, request);
            model.addAttribute("roles", userService.getAllRoles());
            model.addAttribute("userDTO", userService.createUserDTO(username));
            model.addAttribute("countries", userService.getAllCountries());
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "edit-user";
    }

    @PostMapping("/users/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editUser(@ModelAttribute UserDTO userDTO, BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "edit-user";
        }

        userService.checkForUpdateUserFromDTOThrowing(userDTO, userService, LOGGER, exceptionMsg);

        return "redirect:/admin/users";
    }

    @PostMapping("/users/{username}/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteUser(@PathVariable String username) {
        try {
            userService.removeUser(username);
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "redirect:/admin/users";
    }

    @PostMapping("/users/{username}/restore")
    @PreAuthorize("hasRole('ADMIN')")
    public String restoreUser(@PathVariable String username) {
        try {
            userService.restoreUser(username);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }
        return "redirect:/admin/users";
    }

    @GetMapping("/playlists")
    @PreAuthorize("hasRole('ADMIN')")
    public String allPlaylists(Model model, HttpServletRequest request) {
        userService.addLoggedInUserAtribute(model, request);
        try {
            model.addAttribute("playlists", playlistService.getAllPlaylists());
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }
        return "playlists-list";
    }

    @GetMapping("/playlists/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editPlaylistForm(Model model, @PathVariable long id, HttpServletRequest request) {
        userService.addLoggedInUserAtribute(model, request);
        try {
            Playlist playlist = playlistService.getPlaylist(id);
            PlaylistDTO playlistDTO = new PlaylistDTO();
            playlistDTO.setId(playlist.getId());
            playlistDTO.setTitle(playlist.getTitle());

            model.addAttribute("playlist", playlistDTO);
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "edit-playlist-admin";
    }

    @PostMapping("/playlists/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editPlaylist(@ModelAttribute PlaylistDTO playlistDTO, BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "edit-playlist-admin";
        }

        try {
            playlistService.updatePlaylistFromDTO(playlistDTO);

        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "redirect:/admin/playlists";
    }

    @PostMapping("/playlists/{id}/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public String deletePlaylist(@PathVariable long id) {
        try {
            playlistService.deletePlaylist(id);
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "redirect:/admin/playlists";
    }

    @GetMapping("/sync")
    @PreAuthorize("hasRole('ADMIN')")
    public String genreSync() {
        playlistService.syncGenres();
        return "redirect:/admin/users";
    }
}

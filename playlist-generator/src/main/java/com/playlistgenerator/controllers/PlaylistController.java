package com.playlistgenerator.controllers;

import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.PlaylistDTO;
import com.playlistgenerator.models.User;
import com.playlistgenerator.services.contracts.PlaylistService;
import com.playlistgenerator.services.contracts.TravelDurationService;
import com.playlistgenerator.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class PlaylistController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlaylistController.class);
    private static final String exceptionMsg = "There was a problem with the database";

    private PlaylistService playlistService;
    private UserService userService;
    private TravelDurationService travelDurationService;

    @Autowired
    public PlaylistController(PlaylistService playlistService, UserService userService
            , TravelDurationService travelDurationService) {
        this.playlistService = playlistService;
        this.userService = userService;
        this.travelDurationService = travelDurationService;
    }

    @GetMapping("/playlists")
    public String searchPlaylists(Model model, HttpServletRequest request,
                                  @RequestParam(value = "title", required = false, defaultValue = "") String title,
                                  @RequestParam(value = "genres", required = false, defaultValue = "") String genres,
                                  @RequestParam(value = "min-duration", required = false, defaultValue = "") String minDuration,
                                  @RequestParam(value = "max-duration", required = false, defaultValue = "") String maxDuration,
                                  @RequestParam(value = "page", required = false) Optional<Integer> page,
                                  @RequestParam(value = "size", required = false) Optional<Integer> size) {
        LOGGER.info(title);
        LOGGER.info(genres);
        LOGGER.info(minDuration);
        LOGGER.info(maxDuration);
        model.addAttribute("title", title);
        model.addAttribute("genresparam", genres);
        model.addAttribute("minduration", minDuration);
        model.addAttribute("maxduration", maxDuration);
        userService.addLoggedInUserAtribute(model, request);
        model.addAttribute("genres", playlistService.getAllGenres());

        if (minDuration.isEmpty() || Integer.parseInt(minDuration) == 0) {
            minDuration = "";
        }

        if (maxDuration.isEmpty() || Integer.parseInt(maxDuration) == 600) {
            maxDuration = "";
        }

        List<Playlist> playlists = playlistService.searchPlaylists(title, genres, minDuration, maxDuration);
        playlistService.pagination(model, page, size, playlists, playlistService);

        return "browse";
    }

    @GetMapping("/playlists/{playlistId}")
    public String viewPlaylist(@PathVariable long playlistId, Model model, HttpServletRequest request) {
//        userService.addLoggedInUserAtribute(model, request);

        try {
            Playlist playlist = playlistService.getPlaylistWithTracks(playlistId);
            model.addAttribute("playlist", playlist);
            User user = userService.getLoggedInUser(request);

            if (user != null) {
                model.addAttribute("user", user);
                boolean owner = playlist.getUser().equals(user);
                model.addAttribute("owner", owner);
            }
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return "playlist";
    }

    @GetMapping("/create-playlist")
    @PreAuthorize("isAuthenticated()")
    public String createPlaylistForm(Model model, HttpServletRequest request) {
        userService.addLoggedInUserAtribute(model, request);
        PlaylistDTO playlistDTO = new PlaylistDTO();
        int duration = travelDurationService.getTravelDuration();
        if (duration == 0) {
            LOGGER.error("User tried to type /create-playlist without going through the /journey page");
            //405
            throw new ResponseStatusException(
                    HttpStatus.METHOD_NOT_ALLOWED,
                    "You need to enter your journey details"
            );
        }
        playlistDTO.setDuration(duration);

        try {
            model.addAttribute("genres", playlistService.getAllGenres());
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        model.addAttribute("playlist", playlistDTO);

        return "create-playlist";
    }

    @PostMapping("/create-playlist")
    @PreAuthorize("isAuthenticated()")
    public String createPlaylist(@Valid @ModelAttribute PlaylistDTO playlistDTO, HttpServletRequest request,
                                 BindingResult bindingErrors) {
        if (bindingErrors.hasErrors()) {
            LOGGER.error("BindingResult class had issues");
            return "create-playlist";
        }

        long id;
        try {
            id = playlistService.createPlaylist(playlistDTO, userService.getLoggedInUser(request));
        } catch (DataIntegrityViolationException div) {
            LOGGER.error(div.getMessage());
            return "create-playlist-error";
        } catch (IllegalArgumentException ie) {
            LOGGER.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg
            );
        }

        return String.format("redirect:/playlists/%d", id);
    }
}

package com.playlistgenerator.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "genres")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "genre_id")
    private long genreId;

    @Column(name = "name")
    private String name;

    @JsonProperty("id")
    @Column(name = "deezer_id")
    private long deezerId;

    public Genre() {
    }

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long id) {
        this.genreId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDeezerId() {
        return deezerId;
    }

    public void setDeezerId(long deezerId) {
        this.deezerId = deezerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return getDeezerId() == genre.getDeezerId() &&
                getName().equals(genre.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDeezerId());
    }
}

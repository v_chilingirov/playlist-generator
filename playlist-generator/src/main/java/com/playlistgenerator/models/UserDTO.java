package com.playlistgenerator.models;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class UserDTO {

    @Size(min = 1, max = 50, message = "Username cannot be more than 50 symbols")
    private String username;

    @Size(min = 6, max = 100, message = "Password must be between 6 and 100 symbols")
    private String password;

    @Size(min = 5, max = 100, message = "Email must be between 5 and 100 symbols")
    private String email;

    private List<String> roles;
    private long countryId;

    public UserDTO() {
        roles = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return new ArrayList<>(roles);
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void addRole(String role) {
        roles.add(role);
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
}

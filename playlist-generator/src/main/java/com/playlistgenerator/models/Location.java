package com.playlistgenerator.models;

public class Location {

    private String CountryRegion;
    private String AdminDistrict;
    private String Locality;
    private String PostalCode;
    private String AddressLine;

    public Location() {
    }

    public String getCountryRegion() {
        return CountryRegion;
    }

    public void setCountryRegion(String countryRegion) {
        CountryRegion = countryRegion;
    }

    public String getAdminDistrict() {
        return AdminDistrict;
    }

    public void setAdminDistrict(String adminDistrict) {
        AdminDistrict = adminDistrict;
    }

    public String getLocality() {
        return Locality;
    }

    public void setLocality(String locality) {
        Locality = locality;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getAddressLine() {
        return AddressLine;
    }

    public void setAddressLine(String addressLine) {
        AddressLine = addressLine;
    }
}

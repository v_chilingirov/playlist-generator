package com.playlistgenerator.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "artists")
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "artist_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "artist_tracklist_url")
    private String tracklist;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "artist", orphanRemoval = true)
    private List<Album> albums;

    public Artist() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }

    public List<Album> getAlbums() {
        return new ArrayList<>(albums);
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return id == artist.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

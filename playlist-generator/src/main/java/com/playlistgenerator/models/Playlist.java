package com.playlistgenerator.models;

import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "playlists")
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "playlist_id")
    private long id;

    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "playlists_to_genres",
            joinColumns = @JoinColumn(
                    name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "genre_id"))
    private Set<Genre> genres;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "playlists_to_tracks",
            joinColumns = @JoinColumn(
                    name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "track_id"))
    private Set<Track> tracks;

    @Formula("(SELECT AVG(t.rank) FROM tracks t\n" +
            "JOIN playlists_to_tracks ptt ON t.track_id = ptt.track_id\n" +
            "WHERE ptt.playlist_id = playlist_id)")
    private int rank;

    @Formula("(SELECT SUM(t.duration) FROM tracks t\n" +
            "JOIN playlists_to_tracks ptt ON t.track_id = ptt.track_id\n" +
            "WHERE ptt.playlist_id = playlist_id)")
    private int duration;

    @Lob
    @Column(name = "picture")
    private byte[] picture;

    public Playlist() {
//        genres = new HashSet<>();
        tracks = new HashSet<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Genre> getGenres() {
        return new HashSet<>(genres);
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public Set<Track> getTracks() {
        return new HashSet<>(tracks);
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public void addGenre(Genre genre) {
        genres.add(genre);
    }

    public void removeGenre(Genre genre) {
        genres.remove(genre);
    }

    public void addTrack(Track track) {
        tracks.add(track);
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureAsBase64() {
        if (picture == null) {
            return "";
        }

        return "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(this.getPicture());
    }
}

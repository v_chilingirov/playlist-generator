package com.playlistgenerator.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private long id;

    @Column(name = "country", unique = true)
    @Size(min = 4, max = 100, message = "Country's name must be between 4 and 100 characters long!")
    private String name;

    public Country() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String country) {
        this.name = country;
    }
}
package com.playlistgenerator.models;

import java.util.List;

public class PlaylistDTO {

    private long id;
    private String title;
    private List<String> genres;
    private List<Integer> percentages;
    private int duration;
    private boolean repetitionAllowed;
    private boolean topTracksOnly;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isTopTracksOnly() {
        return topTracksOnly;
    }

    public void setTopTracksOnly(boolean topTracksOnly) {
        this.topTracksOnly = topTracksOnly;
    }

    public boolean isRepetitionAllowed() {
        return repetitionAllowed;
    }

    public void setRepetitionAllowed(boolean repetitionAllowed) {
        this.repetitionAllowed = repetitionAllowed;
    }

    public int getDuration() {
        return duration;
    }

    //setting the duration works in minutes!
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public List<Integer> getPercentages() {
        return percentages;
    }

    public void setPercentages(List<Integer> percentages) {
        this.percentages = percentages;
    }
}

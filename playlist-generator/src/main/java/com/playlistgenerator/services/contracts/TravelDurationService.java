package com.playlistgenerator.services.contracts;

import com.playlistgenerator.models.Journey;

public interface TravelDurationService {

    int getTravelDuration();

    void calculateTravelDuration(Journey journey);
}

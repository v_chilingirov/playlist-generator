package com.playlistgenerator.services.contracts;

public interface PictureService {

    String getRandomPicture();
}

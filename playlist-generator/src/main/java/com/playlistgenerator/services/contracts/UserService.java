package com.playlistgenerator.services.contracts;

import com.playlistgenerator.models.Country;
import com.playlistgenerator.models.Role;
import com.playlistgenerator.models.User;
import com.playlistgenerator.models.UserDTO;
import org.slf4j.Logger;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService extends UserDetailsService {

    List<User> getAllUsers();

    List<User> getAllDeletedUsers();

    User getUser(String username);

    User getUserWithRoles(String username);

    User getUserWithPlaylists(String username);

    UserDTO createUserDTO(String username);

    void updateUserFromDTO(UserDTO userDTO);

    void createUserFromDTO(UserDTO userDTO);

    void removeUser(String username);

    void restoreUser(String username);

    User getLoggedInUser(HttpServletRequest request);

    void addLoggedInUserAtribute(Model model, HttpServletRequest request);

    List<Role> getAllRoles();

    void autoLogin(HttpServletRequest request, String username, String password);

    List<Country> getAllCountries();

    void checkForUpdateUserFromDTOThrowing(@ModelAttribute UserDTO userDTO, UserService userService, Logger logger, String exceptionMsg);
}

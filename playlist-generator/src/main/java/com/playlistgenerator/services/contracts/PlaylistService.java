package com.playlistgenerator.services.contracts;

import com.playlistgenerator.models.Genre;
import com.playlistgenerator.models.Playlist;
import com.playlistgenerator.models.PlaylistDTO;
import com.playlistgenerator.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface PlaylistService {

    Playlist getPlaylist(long id);

    void updatePlaylistFromDTO(PlaylistDTO playlistDTO);

    Playlist getPlaylistWithTracks(long id);

    List<Playlist> getAllPlaylists();

    List<Playlist> searchPlaylists(String title, String genres, String minDuration, String maxDuration);

    List<Genre> getAllGenres();

    long createPlaylist(PlaylistDTO playlistDTO, User user);

    void deletePlaylist(long playlistId);

    void syncGenres();

    void pagination(Model model, @RequestParam(value = "page", required = false) Optional<Integer> page,
                    @RequestParam(value = "size", required = false) Optional<Integer> size,
                    List<Playlist> playlists, PlaylistService playlistService);

    Page<Playlist> findPaginated(Pageable pageable, List<Playlist> playlists);
}

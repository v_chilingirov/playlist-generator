package com.playlistgenerator.services;

import com.playlistgenerator.models.Journey;
import com.playlistgenerator.models.Location;
import com.playlistgenerator.services.contracts.TravelDurationService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@PropertySource("application.properties")
@Service
public class TravelDurationServiceImpl implements TravelDurationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TravelDurationServiceImpl.class);
    private static final String findLocationURL = "https://dev.virtualearth.net/REST/v1/Locations?" +
            "%s" +
            "&key=%s";
    private static final String findTravelDurationURL = "https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins=" +
            "%f,%f" +
            "&destinations=" +
            "%f,%f" +
            "&travelMode=driving&key=" +
            "%s";

    private int travelDuration;
    private String key;

    @Autowired
    public TravelDurationServiceImpl(Environment env) {
        this.key = env.getProperty("bingAPIkey");
    }

    @Override
    public int getTravelDuration() {
        return travelDuration;
    }

    private void setTravelDuration(int travelDuration) {
        if (travelDuration < 0) {
            LOGGER.info("IllegalArgumentException was thrown here: negative travel duration");
            throw new IllegalArgumentException("Travel duration can't be a negative number!");
        }

        this.travelDuration = travelDuration;
    }

    @Override
    public void calculateTravelDuration(Journey journey) {
        List<Double> startLatLong;
        List<Double> endLatLong;

        try {
            startLatLong = getLatLongOfLocation(journey.getStartLocation());
            endLatLong = getLatLongOfLocation(journey.getEndLocation());
        } catch (JSONException jsonExc) {
            setTravelDuration(0);
            LOGGER.info("IllegalArgumentException was thrown here: address doesn't exist");
            throw new IllegalArgumentException("That address doesn't exist");
        }

        String url = String.format(Locale.US, findTravelDurationURL, startLatLong.get(0), startLatLong.get(1), endLatLong.get(0), endLatLong.get(1), key);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);

        JSONObject jsonObject = new JSONObject(response);
        JSONArray results = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("results");

        try {
            Double travelTime = (Double) results.getJSONObject(0).get("travelDuration");
            setTravelDuration(travelTime.intValue());
        } catch (ClassCastException e) {
            LOGGER.warn("API sent 0 to travelTime and because of that it throws ClassCastException");
            setTravelDuration(0);
            throw new IllegalArgumentException("Input different addresses");
        }
    }

    private List<Double> getLatLongOfLocation(Location location) {
        String url = configuringLocationURL(location);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);

        JSONObject jsonObject = new JSONObject(response);
        JSONArray geocodePoints = jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("geocodePoints");

        JSONObject geocodePoint = geocodePoints.getJSONObject(0);
        if (geocodePoints.length() > 1) {
            for (int i = 1; i < geocodePoints.length(); i++) {
                if (geocodePoints.getJSONObject(i).getJSONArray("usageTypes").toList().contains("Route")) {
                    geocodePoint = geocodePoints.getJSONObject(i);
                }
            }
        }

        return geocodePoint.getJSONArray("coordinates")
                .toList()
                .stream()
                .map(Object::toString)
                .mapToDouble(Double::parseDouble)
                .boxed()
                .collect(Collectors.toList());
    }

    private String configuringLocationURL(Location location) {
        StringBuilder queryParameters = new StringBuilder();
        queryParameters.append("countryRegion=")
                .append(location.getCountryRegion())
                .append("&locality=")
                .append(location.getLocality());

        if (!location.getAdminDistrict().equals("")) {
            queryParameters.append("&adminDistrict=")
                    .append(location.getAdminDistrict());
        }

        if (!location.getPostalCode().equals("")) {
            queryParameters.append("&postalCode=")
                    .append(location.getPostalCode());
        }

        if (!location.getAddressLine().equals("")) {
            queryParameters.append("&addressLine=")
                    .append(location.getAddressLine());
        }

        return String.format(findLocationURL, queryParameters, key);
    }
}

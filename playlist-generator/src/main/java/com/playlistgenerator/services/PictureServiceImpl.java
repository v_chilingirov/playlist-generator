package com.playlistgenerator.services;

import com.playlistgenerator.services.contracts.PictureService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@PropertySource("application.properties")
@Service
public class PictureServiceImpl implements PictureService {

    private String key;
    private static final int accessiblePages = 167;
    private static final int picturesPerPage = 3;
    private static final String pictureURLRequest = "https://pixabay.com/api/?key=%s" +
            "&category=music&page=%d" +
            "&per_page=%d";

    @Autowired
    public PictureServiceImpl(Environment env) {
        this.key = env.getProperty("pixabayAPIkey");
    }

    public int getRandomPage() {
        Random random = new Random();
        return random.nextInt(accessiblePages) + 1;
    }

    public int getRandomIndex() {
        Random random = new Random();
        return random.nextInt(picturesPerPage);
    }

    @Override
    public String getRandomPicture() {
        String url = String.format(pictureURLRequest, key, getRandomPage(), picturesPerPage);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);

        JSONObject jsonObject = new JSONObject(response);
        return jsonObject
                .getJSONArray("hits")
                .getJSONObject(getRandomIndex())
                .getString("webformatURL");
    }
}

package com.playlistgenerator.services;

import com.playlistgenerator.exceptions.EmptyDatabaseException;
import com.playlistgenerator.models.*;
import com.playlistgenerator.repositories.contracts.PlaylistRepository;
import com.playlistgenerator.services.contracts.PictureService;
import com.playlistgenerator.services.contracts.PlaylistService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlaylistServiceImpl.class);
    private static final String exceptionMsg = "Exception thrown from PlaylistServiceImpl::%s";
    private static final String exceptionWrongDataMsg = "User inputted wrong data or requested something that it doesn't exist";
    private static final String loggerEmptyDB = "Database is empty";
    private static final String exceptionEmptyDBMsg = "Database cannot be empty!";

    private PlaylistRepository playlistRepository;
    private PictureService pictureService;

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository playlistRepository, PictureService pictureService) {
        this.playlistRepository = playlistRepository;
        this.pictureService = pictureService;
    }

    @Override
    public Playlist getPlaylist(long id) {
        Playlist playlist;
        try {
            playlist = playlistRepository.getPlaylist(id);
            if (playlist == null) {
                LOGGER.warn(exceptionWrongDataMsg);
                throw new IllegalArgumentException(exceptionWrongDataMsg);
            }

            return playlist;
        } catch (IllegalArgumentException ie) {
            throw ie;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getPlaylist"));
        }
    }

    @Override
    public Playlist getPlaylistWithTracks(long id) {
        Playlist playlist;
        try {
            playlist = playlistRepository.getPlaylistWithTracks(id);
            if (playlist == null) {
                LOGGER.warn(exceptionWrongDataMsg);
                throw new IllegalArgumentException(exceptionWrongDataMsg);
            }

            return playlist;
        } catch (IllegalArgumentException ie) {
            throw ie;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getPlaylistWithTracks"));
        }
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        List<Playlist> list;
        try {
            list = playlistRepository.getAllPlaylists();
            if (list == null) {
                LOGGER.error(loggerEmptyDB);
                throw new EmptyDatabaseException(exceptionEmptyDBMsg);
            }

            return list;
        } catch (EmptyDatabaseException ed) {
            throw ed;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllPlaylists"));
        }
    }

    @Override
    public List<Playlist> searchPlaylists(String title, String genres,
                                          String minDuration, String maxDuration) {
        String hqlStart = "SELECT DISTINCT p FROM Playlist p JOIN p.genres g ";
        StringBuilder SQLquery = new StringBuilder();
        SQLquery.append(hqlStart);

        ArrayList<String> words = new ArrayList<>();
        if (!title.isEmpty()) {
            SQLquery.append("WHERE ");
            words.addAll(Arrays.asList(title.split(" ")));
            int initialSize = words.size();
            for (int i = 0; i < initialSize * 4; i += 4) {
                SQLquery.append(String.format("(p.title LIKE :title%d OR ", i));
                SQLquery.append(String.format("p.title LIKE :title%d OR ", i + 1));
                SQLquery.append(String.format("p.title LIKE :title%d OR ", i + 2));
                SQLquery.append(String.format("p.title LIKE :title%d) ", i + 3));
                String newWord1 = "% " + words.get(i) + " %";
                String newWord2 = "% " + words.get(i);
                String newWord3 = words.get(i) + " %";
                words.add(i + 1, newWord1);
                words.add(i + 2, newWord2);
                words.add(i + 3, newWord3);
                if (i + 3 < initialSize * 4 - 1) {
                    SQLquery.append("AND ");
                }
            }
        }

        List<String> genresArr = new ArrayList<>();
        if (!genres.isEmpty()) {
            genresArr = Arrays.asList(genres.split(" "));
            if (!title.isEmpty()) {
                SQLquery.append("AND (");
            } else {
                SQLquery.append("WHERE (");
            }

            for (int i = 0; i < genresArr.size(); i++) {
                SQLquery.append(String.format("g.name LIKE :genre%d ", i));
                if (i < genresArr.size() - 1) {
                    SQLquery.append("OR ");
                }
            }

            SQLquery.append(") ");
        }

        int minDurationInt = 0;
        if (!minDuration.isEmpty()) {
            if (!title.isEmpty() || !genres.isEmpty()) {
                SQLquery.append("AND ");
            } else {
                SQLquery.append("WHERE ");
            }

            SQLquery.append("p.duration >= :min_duration ");
            minDurationInt = Integer.parseInt(minDuration);
            minDurationInt *= 60;
        }

        int maxDurationInt = 0;
        if (!maxDuration.isEmpty()) {
            if (!title.isEmpty() || !genres.isEmpty() || !minDuration.isEmpty()) {
                SQLquery.append("AND ");
            } else {
                SQLquery.append("WHERE ");
            }

            SQLquery.append("p.duration <= :max_duration ");
            maxDurationInt = Integer.parseInt(maxDuration);
            maxDurationInt *= 60;
        }

        String hqlEnd = "ORDER BY p.rank DESC";
        SQLquery.append(hqlEnd);
        LOGGER.info(SQLquery.toString());
        try {
            return playlistRepository.searchPlaylists(SQLquery.toString(), words,
                    genresArr, minDurationInt, maxDurationInt);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "searchPlaylists"));
        }
    }

    @Override
    public List<Genre> getAllGenres() {
        List<Genre> list;
        try {
            list = playlistRepository.getAllGenres();
            if (list == null) {
                LOGGER.error(loggerEmptyDB);
                throw new EmptyDatabaseException(exceptionEmptyDBMsg);
            }

            return list;
        } catch (EmptyDatabaseException ed) {
            throw ed;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllGenres"));
        }
    }

    @Override
    public long createPlaylist(PlaylistDTO playlistDTO, User user) {
        Playlist playlist = convertDTOToNewPlaylist(playlistDTO, user);

        Map<Genre, Integer> genresDuration = calculatingGenresDuration(playlistDTO);
        Set<Artist> selectedArtists = new HashSet<>();

        Set<Track> tracks = new HashSet<>(
                generateRandomTracks(genresDuration, selectedArtists, playlistDTO.isRepetitionAllowed(),
                        playlistDTO.isTopTracksOnly())
        );

        addTracksToPlaylist(playlist, tracks);
        long id = addPlaylistToDatabase(playlist);
        LOGGER.info("A new playlist with the name \"{}\" was created", playlist.getTitle());
        LOGGER.info("Total duration of the new playlist is: {} minutes", playlist.getDuration() / 60);
        LOGGER.info("With {} songs inside", tracks.size());

        return id;
    }

    public Playlist convertDTOToNewPlaylist(PlaylistDTO playlistDTO, User user) {
        Playlist playlist = new Playlist();
        playlist.setGenres(new HashSet<>());
        playlist.setTitle(playlistDTO.getTitle());
        playlistDTO.getGenres().forEach(genre -> playlist.addGenre(getGenreByName(genre)));
        addPictureToPlaylist(playlist);
        playlist.setUser(user);
        return playlist;
    }


    public Map<Genre, Integer> calculatingGenresDuration(PlaylistDTO playlistDTO) {
        Map<Genre, Integer> genresDuration = new HashMap<>();
        List<Genre> genres = getAllGenres();
        for (int i = 0; i < genres.size(); i++) {
            if (playlistDTO.getPercentages().get(i) == null) {
                continue;
            }

            float percentage = ((float) playlistDTO.getPercentages().get(i)) / 100;
            int duration = (int) (playlistDTO.getDuration() * 60 * percentage);
            genresDuration.put(genres.get(i), duration);
        }

        return genresDuration;
    }

    public Set<Track> generateRandomTracks(Map<Genre, Integer> genresDuration, Set<Artist> selectedArtists,
                                           boolean repetitionAllowed, boolean topTracksOnly) {
        Set<Track> selectedTracks = new HashSet<>();
        int floorLimit = -300;
        int ceilingLimit = 300;

        for (Genre genre : genresDuration.keySet()) {
            if (genresDuration.get(genre) == 0) {
                continue;
            }

            List<Track> tracks;
            try {
                if (topTracksOnly) {
                    tracks = playlistRepository.getTopTracksFromGenre(genre);
                } else {
                    tracks = playlistRepository.getAllTracksFromGenre(genre);
                    Collections.shuffle(tracks);
                }

                if (tracks == null) {
                    LOGGER.error(loggerEmptyDB);
                    throw new EmptyDatabaseException(exceptionEmptyDBMsg);
                }
            } catch (EmptyDatabaseException ed) {
                throw ed;
            } catch (RuntimeException e) {
                LOGGER.warn(e.getMessage());
                throw new RuntimeException(String.format(exceptionMsg, "generateRandomTracks"));
            }

            int currentGenreDuration = genresDuration.get(genre);

            int realDuration = 0;
            for (Track track : tracks) {
                int possibleDuration = realDuration + track.getDuration();
                if (possibleDuration > currentGenreDuration + ceilingLimit) {
                    continue;
                }

                if (!repetitionAllowed) {
                    Artist artist = track.getAlbum().getArtist();
                    if (selectedArtists.contains(artist)) {
                        continue;
                    }

                    selectedArtists.add(artist);
                }

                selectedTracks.add(track);
                realDuration += track.getDuration();
                LOGGER.info("{} - {} was added to the playlist", track.getAlbum().getArtist().getName(), track.getTitle());
                LOGGER.info("from genre \"{}\" and with {} minutes", genre.getName(), track.getDuration() / 60);

                if ((realDuration >= currentGenreDuration + floorLimit) &&
                        (realDuration <= currentGenreDuration + ceilingLimit)) {
                    int difference = currentGenreDuration - realDuration;
                    floorLimit += difference;
                    ceilingLimit += difference;
                    break;
                }
            }
        }

        return selectedTracks;
    }

    public void addPictureToPlaylist(Playlist playlist) {
        try {
            URL url = new URL(pictureService.getRandomPicture());
            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
            httpcon.addRequestProperty("User-Agent", "Mozilla/4.0");
            httpcon.getInputStream();
            byte[] picture = IOUtils.toByteArray(httpcon);
            BASE64Encoder encoder = new BASE64Encoder();
            encoder.encode(picture);
            playlist.setPicture(picture);
        } catch (IOException e) {
            LOGGER.warn(e.getMessage());
            playlist.setPicture(null);
        }
    }

    public void addTracksToPlaylist(Playlist playlist, Set<Track> tracks) {
        tracks.forEach(playlist::addTrack);

        int totalDuration = 0;
        for (Track track : tracks) {
            totalDuration += track.getDuration();
        }
        playlist.setDuration(totalDuration);
    }

    private long addPlaylistToDatabase(Playlist playlist) {
        long id;
        try {
            id = playlistRepository.createPlaylist(playlist);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "addPlaylistToDatabase"));
        }

        return id;
    }

    @Override
    public void updatePlaylistFromDTO(PlaylistDTO playlistDTO) {
        Playlist playlist = getPlaylist(playlistDTO.getId());
        playlist.setTitle(playlistDTO.getTitle());
        try {
            playlistRepository.updatePlaylist(playlist);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "updatePlaylistFromDTO"));
        }
    }

    @Override
    public void deletePlaylist(long playlistId) {
        try {
            playlistRepository.deletePlaylist(getPlaylist(playlistId));
        } catch (IllegalArgumentException ie) {
            throw new IllegalArgumentException(exceptionWrongDataMsg);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "deletePlaylist"));
        }
    }

    @Scheduled(initialDelay = 10000, fixedDelay = 3600000)
    @Override
    public void syncGenres() {
        LOGGER.info("syncGenres has just started");
        playlistRepository.syncGenres();
    }

    @Override
    public void pagination(Model model,
                           @RequestParam(value = "page", required = false) Optional<Integer> page,
                           @RequestParam(value = "size", required = false) Optional<Integer> size,
                           List<Playlist> playlists, PlaylistService playlistService) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(12);
        Page<Playlist> playlistPage = playlistService.findPaginated(PageRequest.of(currentPage - 1, pageSize), playlists);
        model.addAttribute("playlistPage", playlistPage);

        int totalPages = playlistPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
    }

    @Override
    public Page<Playlist> findPaginated(Pageable pageable, List<Playlist> playlists) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Playlist> list;

        if (playlists.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, playlists.size());
            list = playlists.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), playlists.size());
    }

    private Genre getGenreByName(String genreName) {
        Genre genre;
        try {
            genre = playlistRepository.getGenreByName(genreName);
            if (genre == null) {
                LOGGER.warn(exceptionWrongDataMsg);
                throw new IllegalArgumentException(exceptionWrongDataMsg);
            }

            return genre;
        } catch (IllegalArgumentException ie) {
            throw ie;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getGenreByName"));
        }
    }
}

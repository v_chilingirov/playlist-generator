package com.playlistgenerator.services;

import com.playlistgenerator.exceptions.EmptyDatabaseException;
import com.playlistgenerator.models.Country;
import com.playlistgenerator.models.Role;
import com.playlistgenerator.models.User;
import com.playlistgenerator.models.UserDTO;
import com.playlistgenerator.repositories.contracts.UserRepository;
import com.playlistgenerator.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final String exceptionMsg = "Exception thrown from UserServiceImpl::%s";
    private static final String exceptionWrongDataMsg = "User inputted wrong data or requested something that it doesn't exist";
    private static final String loggerEmptyDB = "Database is empty";
    private static final String exceptionEmptyDBMsg = "Database cannot be empty!";

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public List<User> getAllUsers() {
        List<User> list;
        try {
            list = userRepository.getAllUsers();
            if (list == null) {
                LOGGER.error(loggerEmptyDB);
                throw new EmptyDatabaseException(exceptionEmptyDBMsg);
            }

            return list;
        } catch (EmptyDatabaseException ed) {
            throw ed;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllUsers"));
        }
    }

    @Override
    public List<User> getAllDeletedUsers() {
        try {
            return userRepository.getAllDeletedUsers();
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllDeletedUsers"));
        }
    }

    @Override
    public User getUser(String username) {
        User user;
        try {
            user = userRepository.getUser(username);
            if (user == null) {
                throw new IllegalArgumentException();
            }

            return user;
        } catch (IllegalArgumentException ie) {
            LOGGER.warn(exceptionWrongDataMsg);
            throw new IllegalArgumentException("Wrong username");
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUser"));
        }
    }

    @Override
    public User getUserWithRoles(String username) {
        User user;
        try {
            user = userRepository.getUserWithRoles(username);
            if (user == null) {
                throw new IllegalArgumentException();
            }

            return user;
        } catch (IllegalArgumentException ie) {
            LOGGER.warn(exceptionWrongDataMsg);
            throw new IllegalArgumentException("Wrong username");
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUserWithRoles"));
        }
    }

    @Override
    public User getUserWithPlaylists(String username) {
        User user;
        try {
            user = userRepository.getUserWithPlayists(username);
            if (user == null) {
                throw new IllegalArgumentException();
            }

            return user;
        } catch (IllegalArgumentException ie) {
            LOGGER.warn(exceptionWrongDataMsg);
            throw new IllegalArgumentException("Wrong username");
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getUserWithPlaylists"));
        }
    }

    @Override
    public UserDTO createUserDTO(String username) {
        User user = getUserWithRoles(username);
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(username);
        userDTO.setEmail(user.getEmail());
        userDTO.setCountryId(user.getCountry().getId());
        user.getAuthorities().forEach(role -> userDTO.addRole(role.getAuthority()));
        return userDTO;
    }

    @Override
    public void updateUserFromDTO(UserDTO userDTO) {
        User user = getUserWithRoles(userDTO.getUsername());
        user.setEmail(userDTO.getEmail());
        user.setCountry(userRepository.getCountry(userDTO.getCountryId()));
        if (!userDTO.getRoles().isEmpty()) {
            user.setRoles(new HashSet<>());
            userDTO.getRoles().forEach(role -> user.addRole(getRoleByName(role)));
        }
        updateUser(user);
    }

    @Override
    public void createUserFromDTO(UserDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(passwordEncoder().encode(userDTO.getPassword()));
        user.setEmail(userDTO.getEmail());
        user.setCountry(userRepository.getCountry(userDTO.getCountryId()));
        Role role = getRoleByName("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        try {
            userRepository.createUser(user);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "createUser"));
        }
    }

    @Override
    public void removeUser(String username) {
        User user = getUser(username);
        user.setEnabled(false);
        updateUser(user);
    }

    @Override
    public void restoreUser(String username) {
        User user;
        try {
            user = userRepository.getDeletedUser(username);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "restoreUser"));
        }

        user.setEnabled(true);
        updateUser(user);
    }

    @Override
    public User getLoggedInUser(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            return getUser(principal.getName());
        } else {
            return null;
        }
    }

    @Override
    public void addLoggedInUserAtribute(Model model, HttpServletRequest request) {
        if (request.getUserPrincipal() != null) {
            model.addAttribute("user", getLoggedInUser(request));
        }
    }

    @Override
    public List<Role> getAllRoles() {
        List<Role> list;
        try {
            list = userRepository.getAllRoles();
            if (list == null) {
                LOGGER.error(loggerEmptyDB);
                throw new EmptyDatabaseException(exceptionEmptyDBMsg);
            }

            return list;
        } catch (EmptyDatabaseException ed) {
            throw ed;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllRoles"));
        }
    }

    @Override
    public void autoLogin(HttpServletRequest request, String username, String password) {
        try {
            request.login(username, password);
        } catch (ServletException e) {
            LOGGER.error("Error while login ", e);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return getUserWithRoles(username);
    }

    private void updateUser(User user) {
        try {
            userRepository.updateUser(user);
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "updateUser"));
        }
    }

    private Role getRoleByName(String roleName) {
        Role role;
        try {
            role = userRepository.getRoleByName(roleName);
            if (role == null) {
                throw new IllegalArgumentException();
            }

            return role;
        } catch (IllegalArgumentException ie) {
            LOGGER.warn(exceptionWrongDataMsg);
            throw new IllegalArgumentException("Wrong role");
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getRoleByName"));
        }
    }

    @Override
    public List<Country> getAllCountries() {
        List<Country> list;
        try {
            list = userRepository.getAllCountries();
            if (list == null) {
                LOGGER.error(loggerEmptyDB);
                throw new EmptyDatabaseException(exceptionEmptyDBMsg);
            }

            return list;
        } catch (EmptyDatabaseException ed) {
            throw ed;
        } catch (RuntimeException e) {
            LOGGER.warn(e.getMessage());
            throw new RuntimeException(String.format(exceptionMsg, "getAllCountries"));
        }
    }

    @Override
    public void checkForUpdateUserFromDTOThrowing(@ModelAttribute UserDTO userDTO, UserService userService, Logger logger, String exceptionMsg) {
        try {
            userService.updateUserFromDTO(userDTO);
        } catch (IllegalArgumentException ie) {
            logger.error(ie.getMessage());
            //400
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    ie.getMessage()
            );
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            //500
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exceptionMsg);
        }
    }
}

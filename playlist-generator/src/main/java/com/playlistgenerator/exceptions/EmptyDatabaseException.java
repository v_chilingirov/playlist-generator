package com.playlistgenerator.exceptions;

public class EmptyDatabaseException extends RuntimeException {

    public EmptyDatabaseException(String errorMessage) {
        super(errorMessage);
    }
}

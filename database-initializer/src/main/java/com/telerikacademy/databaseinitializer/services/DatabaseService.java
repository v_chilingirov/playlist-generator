package com.telerikacademy.databaseinitializer.services;

public interface DatabaseService {

    void initializeDatabase() throws InterruptedException;
}

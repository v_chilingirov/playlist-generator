package com.telerikacademy.databaseinitializer.services;

import com.telerikacademy.databaseinitializer.models.*;
import com.telerikacademy.databaseinitializer.repositories.DatabaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class DatabaseServiceImpl implements DatabaseService {

    private static final String[] genres = {"Rock", "Dance", "R&B"};

    private Map<String, Artist> allArtists = new HashMap<>();
    private Set<Album> allAlbums = new HashSet<>();
    private Set<Track> allTracks = new HashSet<>();

    private DatabaseRepository databaseRepository;
    private RestTemplate restTemplate;

    @Autowired
    public DatabaseServiceImpl(DatabaseRepository databaseRepository) {
        this.databaseRepository = databaseRepository;
//        this.restTemplate = new RestTemplateBuilder().build();
        this.restTemplate = new RestTemplate();
    }

    public List<Genre> getGenres(RestTemplate restTemplate) {
        GenreList genreList = restTemplate.getForObject("https://api.deezer.com/genre", GenreList.class);
        return genreList.getData();
    }

    public void addGenres(List<Genre> genres) {
        genres.forEach(genre -> databaseRepository.addGenre(genre));
    }

    public void fillAllGenres(RestTemplate restTemplate) {
        addGenres(getGenres(restTemplate));
    }

    public List<Playlist> getPlaylists(RestTemplate restTemplate, String genre) {
        PlaylistList response = restTemplate.getForObject
                (String.format("https://api.deezer.com/search/playlist?q=%s&limit=40", genre), PlaylistList.class);
        return response.getData();
    }

    public void addTracksFromPlaylist(RestTemplate restTemplate, Playlist playlist, Genre genre) {
        TrackList trackList = restTemplate.getForObject(String.format("%s?limit=100", playlist.getTracklist()), TrackList.class);
        List<Track> tracks = trackList.getData();
        for (Track track : tracks) {

            if (allTracks.contains(track) || track.getAlbum().getTracklist().isEmpty()||track.getPreview().isEmpty()) {
                continue;
            }

            track.setGenre(genre);
            allTracks.add(track);

            Album album = track.getAlbum();
            if (allAlbums.contains(album)) {
                if (allArtists.containsKey(track.getArtist().getTracklist())) {
                    Artist artist = allArtists.get(track.getArtist().getTracklist());
                    if (artist.getAlbums().contains(album)) {
                        int index = artist.getAlbums().indexOf(album);
                        artist.getAlbums().get(index).addTrack(track);
                        continue;
                    }
                }
                allTracks.remove(track);
                continue;
            }
            album.addTrack(track);
            allAlbums.add(album);

            Artist artist = track.getArtist();
            if (allArtists.containsKey(artist.getTracklist())) {
                artist = allArtists.get(artist.getTracklist());
                artist.addAlbum(album);
                continue;
            }
            artist.addAlbum(album);
            allArtists.put(artist.getTracklist(), artist);
        }
    }

    public void fillAllTracks(RestTemplate restTemplate, Genre genre) throws InterruptedException {
        List<Playlist> playlists = getPlaylists(restTemplate, genre.getName());
        int count = 1;
        System.out.println(genre.getName());
        for (Playlist playlist : playlists) {
            System.out.println(count);
            count++;
            addTracksFromPlaylist(restTemplate, playlist, genre);
            if (count % 20 == 0) {
                Thread.sleep(5000);
            }
        }
    }

    @PostConstruct
    @Override
    public void initializeDatabase() throws InterruptedException {
        fillAllGenres(restTemplate);

        System.out.println("Downloading tracks...");

        for (String genreName : genres) {
            Genre genre = databaseRepository.getGenreByName(genreName);
            fillAllTracks(restTemplate, genre);
        }

        System.out.println("Artists: " + allArtists.size());
        System.out.println("Albums: " + allAlbums.size());
        System.out.println("Tracks: " + allTracks.size());

        System.out.println("Adding to database...");

        for (Artist artist : allArtists.values()) {
            databaseRepository.addArtist(artist);
        }
        System.out.println("Finished");
    }

}

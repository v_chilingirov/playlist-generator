package com.telerikacademy.databaseinitializer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseInitializerApplication {

    public static void main(String args[]) {
        SpringApplication.run(DatabaseInitializerApplication.class, args);
    }

}

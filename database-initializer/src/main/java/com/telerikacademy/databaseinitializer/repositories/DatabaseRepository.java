package com.telerikacademy.databaseinitializer.repositories;

import com.telerikacademy.databaseinitializer.models.Album;
import com.telerikacademy.databaseinitializer.models.Artist;
import com.telerikacademy.databaseinitializer.models.Genre;
import com.telerikacademy.databaseinitializer.models.Track;

public interface DatabaseRepository {

    boolean containsTrack (Track track);

    void addTrack (Track track);

    void addGenre(Genre genre);

    Genre getGenreById(long id);

    Genre getGenreByName(String name);

    boolean containsAlbum(Album album);

    Album getAlbumByTracklist(String tracklist);

    void addAlbum(Album album);

    boolean containsArtist(Artist artist);

    Artist getArtistByTracklist(String tracklist);

    void addArtist(Artist artist);
}

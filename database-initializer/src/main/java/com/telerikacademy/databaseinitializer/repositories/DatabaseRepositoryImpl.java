package com.telerikacademy.databaseinitializer.repositories;

import com.telerikacademy.databaseinitializer.models.Album;
import com.telerikacademy.databaseinitializer.models.Artist;
import com.telerikacademy.databaseinitializer.models.Genre;
import com.telerikacademy.databaseinitializer.models.Track;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DatabaseRepositoryImpl implements DatabaseRepository {

    private SessionFactory sessionFactory;

    @Autowired
    protected DatabaseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public boolean containsTrack(Track track) {
        try (Session session = sessionFactory.openSession()) {
            Track existingTrack = session.createQuery("FROM Track WHERE link = :link", Track.class)
                    .setParameter("link", track.getLink())
                    .uniqueResult();
            return existingTrack != null;
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void addTrack(Track track) {
        try (Session session = sessionFactory.openSession()) {
            session.save(track);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void addGenre(Genre genre) {
        try (Session session = sessionFactory.openSession()) {
            session.save(genre);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Genre getGenreById(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Genre WHERE id = :id", Genre.class)
                    .setParameter("id", id)
                    .uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Genre getGenreByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Genre genre = new Genre();
            return session.createQuery("FROM Genre WHERE name = :name", Genre.class)
                    .setParameter("name", name)
                    .uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public boolean containsAlbum(Album album) {
        try (Session session = sessionFactory.openSession()) {
            Album existingAlbum = session.createQuery("FROM Album WHERE tracklist = :tracklist", Album.class)
                    .setParameter("tracklist", album.getTracklist())
                    .uniqueResult();
            return existingAlbum != null;
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Album getAlbumByTracklist(String tracklist) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Album WHERE tracklist = :tracklist", Album.class)
                    .setParameter("tracklist", tracklist)
                    .uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void addAlbum(Album album) {
        try (Session session = sessionFactory.openSession()) {
            session.save(album);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public boolean containsArtist(Artist artist) {
        try (Session session = sessionFactory.openSession()) {
            Artist existingArtist = session.createQuery("FROM Artist WHERE tracklist = :tracklist", Artist.class)
                    .setParameter("tracklist", artist.getTracklist())
                    .uniqueResult();
            return existingArtist != null;
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Artist getArtistByTracklist(String tracklist) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Artist WHERE tracklist = :tracklist", Artist.class)
                    .setParameter("tracklist", tracklist)
                    .uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void addArtist(Artist artist) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(artist);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }


}

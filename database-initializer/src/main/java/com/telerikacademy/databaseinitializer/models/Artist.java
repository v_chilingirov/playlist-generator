package com.telerikacademy.databaseinitializer.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "artists")
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "artist_id")
    private long artistId;

    @Column(name = "name")
    private String name;

    @Column(name = "artist_tracklist_url")
    private String tracklist;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "artist", orphanRemoval = true)
    private List<Album> albums = new ArrayList<>();

    public void addAlbum (Album album){
        albums.add(album);
        album.setArtist(this);
    }

    public void removeAlbum (Album album){
        album.setArtist(null);
        albums.remove(album);
    }

    public Artist() {
    }

    public long getArtistId() {
        return artistId;
    }

    public void setArtistId(long artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return tracklist.equals(artist.tracklist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tracklist);
    }

    @Override
    public String toString() {
        return "Artist{" +
                "artistId=" + artistId +
                ", name='" + name + '\'' +
                ", tracklist='" + tracklist + '\'' +
                ", albums=" + albums +
                '}';
    }
}

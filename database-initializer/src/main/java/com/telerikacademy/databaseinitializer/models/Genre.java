package com.telerikacademy.databaseinitializer.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "genres")
public class Genre {

    //TODO Use @Scheduled annotation to auto-update
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "genre_id")
    private long genreId;

    @Column(name = "name")
    private String name;

    @JsonProperty("id")
    @Column(name = "deezer_id")
    private long deezerId;

    public Genre() {
    }

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDeezerId() {
        return deezerId;
    }

    public void setDeezerId(long deezerId) {
        this.deezerId = deezerId;
    }
}

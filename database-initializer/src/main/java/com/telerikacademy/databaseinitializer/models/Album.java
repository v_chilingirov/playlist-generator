package com.telerikacademy.databaseinitializer.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "albums")
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "album_id")
    private long albumId;

    @Column(name = "title")
    private String title;

    @Column(name = "album_tracklist_url")
    private String tracklist;

    @ManyToOne
    @JoinColumn(name = "artist_id")
    private Artist artist;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "album", orphanRemoval = true)
    private List<Track> tracks = new ArrayList<>();

    public Album() {
    }

    public void addTrack (Track track){
        tracks.add(track);
        track.setAlbum(this);
    }

    public void removeTrack (Track track){
        track.setAlbum(null);
        tracks.remove(track);
    }

    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return tracklist.equals(album.tracklist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tracklist);
    }

    @Override
    public String toString() {
        return "Album{" +
                "albumId=" + albumId +
                ", title='" + title + '\'' +
                ", tracklist='" + tracklist + '\'' +
                ", artist=" + artist +
                ", tracks=" + tracks +
                '}';
    }
}
